# laravel-admin-v1

#### 介绍
- 使用laravel开发后台管理系统.版本V1
- laravel 使用版本5.8
	- php >= 7.1.3
	- OpenSSl PHP拓展
	- Mbstring PHP拓展
	- Tokenizer PHP拓展
	- XML PHP拓展
	- Ctype PHP拓展
	- JSON PHP拓展
	- BCMath PHP拓展
- Ubuntu 18.04 php拓展
```
apt install php7.2-mysql 
apt install php7.2-fpm 
``` 

### 软件架构
#### 简单的后台管理系统
- 后台管理员
- 角色管理
- 日志管理
- 菜单管理
- 配置管理

#### 前端使用
- 模块网址：http://ace.jeka.by/index.html
- `public/assets` 后台前端 样式
    - `demo-html.tar.gz` 前端页面，解压后将内容复制到`public/assets`目录下可浏览器打开访问静态页面.

## 安装教程

### 安装方式一

#### 安装前准备
```
chmod 777 -R ./storage         // 需要可写权限
chmod 777 -R ./bootstrap/cache        // 需要可写权限 

# 根目录下 修改.env配置文件，添加写入权限
cp .env.example .env
chmod 777 .env
```
- 已安装 `Composer`
    - 切换到项目根目录。将`composer.lock`删除，执行 `composer install` 安装第三方库。将会在根目录下创建`vendor`文件夹
- 安装 `Composer`; 这里系统 Ubuntu18.04
	- 下载 Composer
	```
	curl -sS https://getcomposer.org/installer | php
	```
	- 查看 Composer 版本
	```
	php composer.phar --version
	```
	- Composer 设置系统变量
	```
	mv composer.phar /usr/local/bin/composer
	```
	- 使用系统变量查看 Composer 版本
	```
	composer --version
	```
	- 或者执行 `composer` 出现介绍

#### 安装
- 1、配置好域名后,指向`public`.
- 2、将`public/install/install.lock`删除，访问域名安装.

- 软链接laravel文件存储 `storage/app/public` 地址到 `public/` 下创建 `public/storage`
```
php artisan storage:link
```
	- linux 执行示例
	```
	ln -s /work/www/laravel-admin-v1/storage/app/public /work/www/laravel-admin-v1/public/storage
	```

### 安装方式二
```
chmod 777 -R ./storage         // 需要可写权限
chmod 777 -R ./bootstrap/cache        // 需要可写权限 
```
- 配置laravel5.8运行环境
- 已安装 `Composer`
    - 执行 `composer install` 安装第三方库。将会在根目录下创建`vendor`文件夹
- 文件储存
    - 软链接laravel文件存储 `storage/app/public` 地址到 `public/` 下创建 `public/storage`
    ```
    php artisan storage:link
    ```
		- linux 执行示例
		```
		ln -s /work/www/laravel-admin-v1/storage/app/public /work/www/laravel-admin-v1/public/storage
		```

### 安装补助
- 如果 `vendor` 失败;可尝试解压项目根下面 `vendor.tar.gz`;
    - 使用解压 `vendor.tar.gz` 后台登陆可以"记住我"勾选操作;

#### nginx配置
- nginx.conf
```
	upstream php7.2.24{
		server 127.0.0.1:9000;
	}
```
```
server {
   listen 80;
   server_name localhost;
   root /work/www/laravel-admin-v1/public;
   index index.php index.html index.htm

   location / {
      try_files $uri $uri/ /index.php?$query_string;
   }

   location ~ \.php$ {
      fastcgi_pass   php7.2.24;
      fastcgi_index  index.php;
      fastcgi_split_path_info ^((?U).+\.php)(/?.+)$;
      fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
      fastcgi_param PATH_INFO $fastcgi_path_info;
      fastcgi_param PATH_TRANSLATED $document_root$fastcgi_path_info;
      include       fastcgi_params;
   }

   location ~ .*\.(gif|jpg|jpeg|png|bmp|swf)$ {
      expires     30d;
   }

   location ~ .*\.(js|css)?$ {
      expires     12h;
   }
}
```

#### .env 示例
```
APP_NAME=Laravel
APP_ENV=local
APP_KEY=base64:xRf8pY5lWMRbvBAg3VS4DUAGYtV+Aih6wHDnyru3jf0=
APP_DEBUG=false
APP_URL=http://localhost

ADMIN_KEY=PBAu3GPUe

LOG_CHANNEL=stack

DB_CONNECTION=mysql
DB_HOST=localhost
DB_PORT=3306
DB_DATABASE=laravel_admin
DB_USERNAME=root
DB_PASSWORD=root
DB_PREFIX=
DB_CHARSET=utf8
DB_COLLATION=utf8_general_ci


BROADCAST_DRIVER=log
CACHE_DRIVER=file
QUEUE_CONNECTION=sync
SESSION_DRIVER=file
SESSION_LIFETIME=120

REDIS_HOST=127.0.0.1
REDIS_PASSWORD=null
REDIS_PORT=6379

MAIL_DRIVER=smtp
MAIL_HOST=smtp.mailtrap.io
MAIL_PORT=2525
MAIL_USERNAME=null
MAIL_PASSWORD=null
MAIL_ENCRYPTION=null

PUSHER_APP_ID=
PUSHER_APP_KEY=
PUSHER_APP_SECRET=
PUSHER_APP_CLUSTER=mt1

MIX_PUSHER_APP_KEY="${PUSHER_APP_KEY}"
MIX_PUSHER_APP_CLUSTER="${PUSHER_APP_CLUSTER}"

SMS_ALIYUN_SIGN_NAME=null
SMS_ALIYUN_KEY_ID=null
SMS_ALIYUN_KEY_SECRET=null
SMS_ALIYUN_VERIFY_CODE=null

```

## 使用说明

### laravel框架命名
- 修改 `.env` 等配置后需要更新/清理配置信息
##### 清理缓存 shell 脚本
``` 
# 需要执行权限 
# chomd +x shell_cache.sh
shell_cache.sh
```

##### 清理配置文件缓存
```
php artisan config:cache
```

##### 清理视图缓存
```
php artisan view:clear
```

##### 清理运行缓存
```
php artisan cache:clear
```

##### 文件储存
- 软链接laravel文件存储 `storage/app/public` 地址到 `public/` 下创建 `public/storage`
    ```
    php artisan storage:link
    ```

##### 重新生成key
``` 
php artisan key:generate 
```

### 后台操作
##### 后台登录 生成登录链接
- 忘记或者后台登录链接错误 访问一下链接提交获取
- `/admin/v1/common/login_token`
    - Username 登录后台的账户
    - Key `.env` 中 `ADMIN_KEY` 后台登录密钥
	

