-- create database `laravel_admin_v002`;
use `laravel_admin_v002`;

/**
后台管理

*/
-- 后台用户
drop table if exists `admin_user`;
CREATE TABLE `admin_user` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL COMMENT '用户名',
  `password` varchar(200) NOT NULL COMMENT '密码',
  `salt` varchar(16) NOT NULL DEFAULT '' COMMENT '加盐',
  `email` varchar(40) DEFAULT NULL COMMENT '邮箱',
  `mobile` varchar(20) DEFAULT NULL COMMENT '电话',
  `realname` varchar(20) DEFAULT NULL COMMENT '真实姓名',
  `head_portrait` varchar(100) DEFAULT NULL COMMENT '头像地址',
  `introduction` varchar(200) DEFAULT NULL COMMENT '简介',
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '状态0-禁用,1正常',
  `level` tinyint(2) NOT NULL DEFAULT '0' COMMENT '等级',
  `is_super` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否为超级管理员1是',
  `creator` int(10) NOT NULL DEFAULT '0' COMMENT '创始人,0初始化',
  `reg_ip` varchar(15) NOT NULL DEFAULT '0.0.0.0' COMMENT '创建IP',
  `remember_token` varchar(100) DEFAULT NULL COMMENT '存储当用户登录应用并勾选「记住我」时的令牌',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `deleted_at` datetime DEFAULT NULL COMMENT '删除时间',
  PRIMARY KEY (`id`),
  unique(`username`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='管理员';
-- admin admin666
-- insert into admin_user(`id`,`username`,`password`,`salt`,`head_portrait`) value(1,'admin','$2y$10$dxy9xreGMzq6KVNztR9kv.62QawzVkHrSKV4YCbUh4VR4Xvp/C5.m','O3G8xrDDJEuFF4mi','/images/yin_yang.gif');

-- 管理员操作日志
drop table if exists `admin_log`;
CREATE TABLE `admin_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menu_id` mediumint(8) NOT NULL DEFAULT '0' COMMENT '菜单id',
  `primary_id` int(11) DEFAULT '0' COMMENT '表中主键ID',
  `method` varchar(10) not null default '' comment '请求方式',
  `query_string` varchar(255) DEFAULT '' COMMENT '请求参数',
  `type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '属性1-记录日志,2-登录,3-退出登录',
  `data` text COMMENT 'POST数据',
  `ip` varchar(18) NOT NULL DEFAULT '',
  `admin_id` mediumint(8) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `idx_menu_id` (`menu_id`),
  KEY `idx_admin_id` (`admin_id`),
  KEY `idx_created_at` (`created_at`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT='管理员操作日志';

-- 配置表
drop table if exists `config`;
create table `config`(
  `name` varchar(30) NOT NULL DEFAULT 'not' COMMENT '配置变量名称',
  `alias` varchar(30) NOT NULL DEFAULT 'not' COMMENT '配置变量别名',
  `value` varchar(225) NOT NULL DEFAULT 'not' COMMENT '配置变量值',
  `type_name` varchar(30) NOT NULL DEFAULT 'not' COMMENT '类型名称',
  `explanation` varchar(30) NOT NULL DEFAULT 'not' COMMENT '配置变量说明',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `deleted_at` datetime DEFAULT NULL COMMENT '删除时间',
  PRIMARY KEY (`name`)
)engine=InnoDB default charset=utf8 comment = '配置表';

-- insert into config(`name`,`alias`,`value`,`type_name`,`explanation`) values
-- ('backstage_name','后台名称','中心数据库后台管理系统','backstage_set',''),
-- ('backstage_icon_url','后台icon','','backstage_set','');

drop table if exists `admin_menu`;
CREATE TABLE `admin_menu` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `menu_name` varchar(30) NOT NULL DEFAULT '' COMMENT '菜单名称',
  `icon` varchar(20) DEFAULT NULL COMMENT '图标',
  `m` varchar(20) NOT NULL DEFAULT 'admin' COMMENT '模板名',
  `v` varchar(20) NOT NULL DEFAULT 'v1' COMMENT '版本',
  `address` varchar(60) NOT NULL DEFAULT '' COMMENT '访问地址',
  `data` varchar(200) DEFAULT NULL COMMENT '额外参数',
  `list_order` smallint(3) NOT NULL DEFAULT '999' COMMENT '排序',
  `parent_id` int(11) NOT NULL DEFAULT '0' COMMENT '父级ID,引用本表id',
  `updated_user` int(11) NOT NULL DEFAULT '0' COMMENT '最近更新操作用户',
  `is_type` tinyint(1) NOT NULL DEFAULT 1 COMMENT '是否显示0-未知,1-导航,2-操作',
  `is_display` tinyint(1) NOT NULL DEFAULT 1 COMMENT '是否显示0-隐藏,1-显示',
  `write_log` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否记录日志0-不记录,1-记录',
  `creator_user` int(11) NOT NULL DEFAULT '0' COMMENT '创造者ID',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`),
  unique key `admin_group` (`m`,`v`,`address`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT='后台菜单';

insert into `admin_menu`(`id`,`icon`,`menu_name`,`address`,`list_order`,`parent_id`,`is_type`,`is_display`,`write_log`) values
(1,'fa-home','主页','home/index',1,0,1,1,0),
(2,'fa-cogs','系统管理','system/index',900,0,1,1,0),
(3,'fa-tachometer','后台菜单管理','system/menu/lists',1,2,1,1,0),
(4,'','创建菜单','system/menu/create',1,3,2,1,1),
(5,'','编辑菜单','system/menu/edit',2,3,2,1,1),
(6,'','删除菜单','system/menu/del',3,3,2,1,1),
(7,'','菜单详情','system/menu/info',4,3,2,1,1),
(8,'fa-cog','系统配置','system/config/index',2,2,1,1,0),
(9,'','配置保存','system/config/save',1,8,2,1,1),
(10,'','清理缓存','system/config/clean_cache',10,8,2,1,1),
(11,'fa-calendar','日志管理','log/index',800,0,1,1,0),
(12,'fa-calendar','管理员日志','admin_user/log/lists',800,11,1,1,0),
(13,'','管理员日志详情','admin_user/log/info',1,12,2,1,0),
(14,'fa-key','管理员','admin_user/index',888,0,1,1,0),

(15,'fa-fire','管理员角色','admin_user/group/lists',2,14,1,1,0),
(16,'','创建管理员角色','admin_user/group/create',1,15,2,1,1),
(17,'','编辑管理员角色','admin_user/group/edit',2,15,2,1,1),
(18,'','删除管理员角色','admin_user/group/del',3,15,2,1,1),
(19,'','管理员详情角色','admin_user/group/info',4,15,2,1,1),

(20,'fa-users','管理员列表','admin_user/index/lists',1,14,1,1,0),
(21,'','创建管理员','admin_user/index/create',1,20,2,1,1),
(22,'','编辑管理员','admin_user/index/edit',2,20,2,1,1),
(23,'','删除管理员','admin_user/index/del',3,20,2,1,1),
(24,'','管理员详情','admin_user/index/info',4,20,2,1,1),
(25,'','编辑密码','admin_user/index/pwd',5,20,2,1,1)
;



drop table if exists `admin_group`;
CREATE TABLE `admin_group` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL DEFAULT '' COMMENT '角色名称',
  `description` varchar(200) DEFAULT NULL COMMENT '角色描述',
  `list_order` smallint(3) NOT NULL DEFAULT '900' COMMENT '排序',
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '状态;0-禁用,1-启用',
  `updated_user` int(11) NOT NULL DEFAULT '0' COMMENT '最近更新操作用户',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='管理员角色组';

drop table if exists `admin_group_access`;
CREATE TABLE `admin_group_access` (
  `admin_id` int(11) NOT NULL DEFAULT '0' COMMENT '管理员ID',
  `group_id` int(11) NOT NULL DEFAULT '0' COMMENT '角色ID',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  unique key `admin_group` (`admin_id`,`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='管理员权限组';


drop table if exists `admin_group_menu`;
CREATE TABLE `admin_group_menu` (
  `group_id` int(11) NOT NULL DEFAULT 0 COMMENT '角色组ID',
  `menu_id` int(11) not null default 0 comment '菜单表ID',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  unique key `group_menu` (`group_id`,`menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='管理员角色组与菜单关联';




