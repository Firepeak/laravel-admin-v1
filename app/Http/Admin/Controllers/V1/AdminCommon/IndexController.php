<?php


namespace App\Http\Admin\Controllers\V1\AdminCommon;


use App\Http\Admin\Controllers\V1\BasisController;
use Illuminate\Support\Str;

class IndexController extends BasisController
{
    /**
     * Todo:: 获取随机数
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\View\View
     */
    public function generateKey(){
        $number = request('number',16);
        $string = Str::random($number);
        return $this->responseMsg('000000',true,'Success',$string);
    }

}
