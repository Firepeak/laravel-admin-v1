<?php


namespace App\Http\Admin\Controllers\V1\AdminCommon;


class MsgController
{
    public function publicIndex()
    {
        //验证参数
        if (!empty(session('msg'))) {
            $data = [
                'msg' => session('msg'),
                'url' => session('url'),
                'wait' => session('wait') ?: 1,
                'code' => session('code') ?: 0,
                'data' => session('data')
            ];
        } else {
            $data = [
                'msg' => '前往首页中...',
                'url' => '/',
                'wait' => 1,
                'code' => 0,
                'data' => '',
            ];
        }
        return view('admin.v1.common.news', ['data' => $data]);
    }

}
