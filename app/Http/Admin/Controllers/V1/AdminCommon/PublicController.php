<?php


namespace App\Http\Admin\Controllers\V1\AdminCommon;


class PublicController
{
    /**
     * Todo:: 生成后台登录链接 token
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function loginToken(){
        if(request()->isMethod('post')){
            $username = trim(request('username'));
            $key = trim(request('key'));
            $token = md5(sha1($username.$key));
            $loginUrl = "/admin?username={$username}&token={$token}";
        }
        return view('admin.v1.common.login_token',compact(array('token','loginUrl')));
    }
}
