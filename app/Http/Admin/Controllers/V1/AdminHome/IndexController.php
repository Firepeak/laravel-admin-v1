<?php


namespace App\Http\Admin\Controllers\V1\AdminHome;


use App\Http\Admin\Controllers\V1\BasisController;

class IndexController extends BasisController
{
    public function publicIndex(){
//        return $this->view([],'admin.v1.adminHome.publicIndex');
        return $this->view([],'adminHome.publicIndex');
    }

}
