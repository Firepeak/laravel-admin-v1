<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class AdminGroup extends Model
{
    public $dateFormat = 'U';
    public $timestamps = false;
    protected $table = 'admin_group';
    public $fillable = ['name', 'description','code'];
    public $messages = [
        'name.required' => '名不能为空',
        'name.max' => '名范围2～20之间',
        'name.min' => '名范围2～20之间',
        'description.required' => '名不能为空',
    ];
    public $rules = [
        'name' => 'required|string|max:20|min:2',
        'description' => 'required',
    ];

    /**
     * Todo:: 状态
     * @param int $code
     * @return array|mixed|string
     */
    public function statusArray($code=-1){
        $data = array(
            0 => '禁用',
            1 => '启用',
        );
        if($code != -1){
            if(isset($data[$code])){
                return $data[$code];
            }else{
                return "";
            }
        }
        return $data;
    }

    /**
     * Todo:: 查询条件
     * @param array $conditions
     * @param array $select
     * @return \Illuminate\Database\Query\Builder
     */
    public function dataWhere($conditions=[],$select=[]){
        $data = DB::table('admin_group')
            ->leftJoin('admin_user','admin_group.updated_user','=','admin_user.id');
        if($select){
            $data->select($select);
        }else{
            $data->select(array(
                'admin_group.*',
                'admin_user.username','admin_user.realname'
            ));
        }
        if(isset($conditions['keyword']) && !empty($conditions['keyword'])){
            $keyword = $conditions['keyword'];
            $data->orWhere(function ($query) use ($keyword) {
                $query->orWhere('admin_group.name', 'like', "%{$keyword}%")
                    ->orWhere('admin_user.username', 'like', "%{$keyword}%");
            });
        }
        if(isset($conditions['id']) && is_numeric($conditions['id'])){
            $data->where('admin_group.id',$conditions['id']);
        }
        if(isset($conditions['name']) && $conditions['name']){
            $data->where('admin_group.name',$conditions['name']);
        }
        if(isset($conditions['status']) && is_numeric($conditions['status'])){
            $data->where('admin_group.status',$conditions['status']);
        }

        if (isset($conditions['start_time']) && !empty($conditions['start_time']) && isset($conditions['end_time']) && !empty($conditions['end_time'])) {
            $data->whereBetween('admin_group.created_at', [$conditions['start_time'],$conditions['end_time']]);
        }else{
            if(isset($conditions['start_time']) && !empty($conditions['start_time'])){
                $data->where('admin_group.created_at','>',$conditions['start_time']);
            }elseif(isset($conditions['end_time']) && !empty($conditions['end_time'])){
                $data->where('admin_group.created_at','<',$conditions['end_time']);
            }
        }
        return $data;
    }


    /**
     * Todo:: 获取列表数据
     * @param array $conditions
     * @param array $select
     * @param array $orderArr
     * @param int $page
     * @param int $pageNum
     * @param string $excelType
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Support\Collection
     */
    public function paginateLists($conditions=[],$select=[],$orderArr=[],$page=1,$pageNum=20,$excelType=''){
        $dataWhere = $this->dataWhere($conditions,$select);
        if ($orderArr) {
            foreach ($orderArr as $order) {
                $dataWhere->orderBy($order['field'], $order['direction']);
            }
        } else {
            $dataWhere->orderBy('admin_group.id', 'desc');
        }
        if($excelType == 'all'){
            // 导出
            $data = $dataWhere->get();
        }else{
            $data = $dataWhere->paginate($pageNum,['*'],'page',$page);
        }
        return $data;
    }

    /**
     * Todo:: 获取分组 菜单Ids
     * @param $groupId
     * @return array
     */
    public function groupMenuIds($groupId){
        $data = DB::table('admin_group_menu')->where('group_id',$groupId)->select('menu_id')->get();
        $ids = [];
        if($data){
            foreach ($data as $key=>$val){
                $ids[] = $val->menu_id;
            }
        }
        return $ids;
    }

    /**
     * Todo:: 组[id=>name]
     * @param int $status
     * @return mixed
     */
    public function groupIdName($status=-1){
        if($status != -1){
            return static::where('status',$status)->pluck('name', 'id')->toArray();
        }else{
            return static::pluck('name', 'id')->toArray();
        }
    }

    public function groupAccess($adminId){
        $data = DB::table('admin_group_access')
            ->join('admin_group','admin_group_access.group_id','=','admin_group.id')
            ->join('admin_user','admin_group_access.admin_id','=','admin_user.id')
            ->select(array(
                'admin_group.id as group_id','admin_group.name as group_name'
            ))->where('admin_user.id','=',$adminId)->pluck('group_name', 'group_id')->toArray();
        return $data;
    }

}
