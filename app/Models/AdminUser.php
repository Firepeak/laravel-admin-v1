<?php


namespace App\Models;


use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;

class AdminUser extends Authenticatable
{
    protected $table = 'admin_user';
    public $timestamps = false; // 指示模型是否自动维护时间戳
    protected $dateFormat = 'U'; // 模型日期列的存储格式。

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * 覆盖Laravel中默认的getAuthPassword方法, 返回用户的password和salt字段
     * @return type
     */
    public function getAuthPassword()
    {
        return ['password' => $this->attributes['password'], 'salt' => $this->attributes['salt']];
    }

    /**
     * Todo:: 状态
     * @param int $code
     * @return array|mixed|string
     */
    public function statusArray($code=-1){
        $data = array(
            0 => '禁用',
            1 => '启用',
        );
        if($code != -1){
            if(isset($data[$code])){
                return $data[$code];
            }else{
                return "";
            }
        }
        return $data;
    }

    /**
     * Todo:: 判断是否为超级管理员
     * @param int $code
     * @return array|mixed|string
     */
    public function isSuperArray($code=-1){
        $data = array(
            0 => '否',
            1 => '是',
        );
        if($code != -1){
            if(isset($data[$code])){
                return $data[$code];
            }else{
                return "";
            }
        }
        return $data;
    }


    /**
     * Todo:: 查询条件
     * @param array $conditions
     * @param array $select
     * @return \Illuminate\Database\Query\Builder
     */
    public function dataWhere($conditions=[],$select=[]){
        $data = DB::table('admin_user')
            ->leftJoin('admin_user as admin_user_02','admin_user.creator','=','admin_user_02.id');
        if($select){
            $data->select($select);
        }else{
            $data->select(array(
                'admin_user.*',
                'admin_user_02.username as creator_username'
            ));
        }
        if(isset($conditions['not_admin']) && $conditions['not_admin']){
            $data->where('admin_user.id','>',1);
        }

        if(isset($conditions['keyword']) && !empty($conditions['keyword'])){
            $keyword = $conditions['keyword'];
            $data->orWhere(function ($query) use ($keyword) {
                $query->orWhere('admin_user.username', 'like', "%{$keyword}%")
                    ->orWhere('admin_user.email', 'like', "%{$keyword}%")
                    ->orWhere('admin_user.mobile', 'like', "%{$keyword}%")
                    ->orWhere('admin_user.realname', 'like', "%{$keyword}%");
            });
        }
        if(isset($conditions['id']) && is_numeric($conditions['id'])){
            $data->where('admin_user.id',$conditions['id']);
        }
        if(isset($conditions['username']) && $conditions['username']){
            $data->where('admin_user.name',$conditions['username']);
        }
        if(isset($conditions['status']) && is_numeric($conditions['status'])){
            $data->where('admin_user.status',$conditions['status']);
        }
        if(isset($conditions['is_super']) && is_numeric($conditions['is_super'])){
            $data->where('admin_user.is_super',$conditions['is_super']);
        }

        if (isset($conditions['start_time']) && !empty($conditions['start_time']) && isset($conditions['end_time']) && !empty($conditions['end_time'])) {
            $data->whereBetween('admin_user.created_at', [$conditions['start_time'],$conditions['end_time']]);
        }else{
            if(isset($conditions['start_time']) && !empty($conditions['start_time'])){
                $data->where('admin_user.created_at','>',$conditions['start_time']);
            }elseif(isset($conditions['end_time']) && !empty($conditions['end_time'])){
                $data->where('admin_user.created_at','<',$conditions['end_time']);
            }
        }
        return $data;
    }


    /**
     * Todo:: 获取列表数据
     * @param array $conditions
     * @param array $select
     * @param array $orderArr
     * @param int $page
     * @param int $pageNum
     * @param string $excelType
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Support\Collection
     */
    public function paginateLists($conditions=[],$select=[],$orderArr=[],$page=1,$pageNum=20,$excelType=''){
        $dataWhere = $this->dataWhere($conditions,$select);
        if ($orderArr) {
            foreach ($orderArr as $order) {
                $dataWhere->orderBy($order['field'], $order['direction']);
            }
        } else {
            $dataWhere->orderBy('admin_user.id', 'desc');
        }
        if($excelType == 'all'){
            // 导出
            $data = $dataWhere->get();
        }else{
            $data = $dataWhere->paginate($pageNum,['*'],'page',$page);
        }
        return $data;
    }




}
