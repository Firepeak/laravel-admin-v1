<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Assets extends Model
{
    protected $table = 'assets';
    public $timestamps = false; // 指示模型是否自动维护时间戳
    protected $dateFormat = 'U'; // 模型日期列的存储格式。
    public $fillable = ['name','name_alias','is_first','status']; //可以注入

    /**
     * Todo:: 是否初始化资产
     * @param string $code
     * @return array|mixed
     */
    public function isFirstArray($code=''){
        $data = [
            0 => '否',
            1 => '是',
        ];
        if($code && isset($data[$code])){
            return $data[$code];
        }
        return $data;
    }

    /**
     * Todo:: 资产状态
     * @param string $code
     * @return array|mixed
     */
    public function statusArray($code=''){
        $data = [
            0 => '禁用',
            1 => '启用',
        ];
        if($code && isset($data[$code])){
            return $data[$code];
        }
        return $data;
    }

    /**
     * Todo:: 名称块
     * @param int $status 状态
     * @param int $is_del 是否删除
     * @return mixed
     */
    public function namePluck($status=-1,$is_del=-1){
        if($status != -1){
            if($is_del != -1){
                return self::where([
                    ['status','=',$status],
                    ['is_del','=',$is_del],
                ])->pluck('name_alias','name')->toArray();
            }else{
                return self::where('status',$status)->pluck('name_alias','name')->toArray();
            }
        }else{
            if($is_del != -1){
                return self::where('is_del',$status)->pluck('name_alias','name')->toArray();
            }
        }
        return self::pluck('name_alias','name')->toArray();
    }

    /**
     * Todo:: 判断是否存在
     * @param $name
     * @return mixed
     */
    public function isName($name){
        return self::where('name',$name)->exists();
    }

    /**
     * Todo:: 通过资产变量名称获取信息
     * @param $name
     * @return mixed
     */
    public function getNameInfo($name){
        return self::where('name',$name)->first();
    }

    /**
     * Todo:: 创建新资产
     * @param $name
     * @param $name_alias
     * @return bool|void
     */
    public function createNew($name,$name_alias){
        if($this->isName($name)){
            return true;
        }else{
            return self::create([
                'name' => $name,
                'name_alias' => $name_alias,
            ]);
        }
    }

    /**
     * Todo:: 查询条件
     * @param array $conditions
     * @param array $select
     * @return \Illuminate\Database\Query\Builder
     */
    public function dataWhere($conditions=[],$select=[]){
        $data = DB::table('assets');
        if($select){
            $data->select($select);
        }else{
            $data->select(array(
                'assets.*',
            ));
        }

        if(isset($conditions['keyword']) && !empty($conditions['keyword'])){
            $keyword = $conditions['keyword'];
            $data->orWhere(function ($query) use ($keyword) {
                $query->orWhere('assets.name', 'like', "%{$keyword}%")
                    ->orWhere('assets.name_alias', 'like', "%{$keyword}%");
            });
        }
        if(isset($conditions['id']) && is_numeric($conditions['id'])){
            $data->where('assets.id',$conditions['id']);
        }
        if(isset($conditions['name']) && $conditions['name']){
            $data->where('assets.name',$conditions['name']);
        }
        if(isset($conditions['name_alias']) && $conditions['name_alias']){
            $data->where('assets.name_alias',$conditions['name_alias']);
        }
        if(isset($conditions['status']) && is_numeric($conditions['status'])){
            $data->where('assets.status',$conditions['status']);
        }
        if(isset($conditions['is_first']) && is_numeric($conditions['is_first'])){
            $data->where('assets.is_first',$conditions['is_first']);
        }
        if(isset($conditions['is_del']) && is_numeric($conditions['is_del'])){
            $data->where('assets.is_del',$conditions['is_del']);
        }

        if (isset($conditions['start_time']) && !empty($conditions['start_time']) && isset($conditions['end_time']) && !empty($conditions['end_time'])) {
            $data->whereBetween('assets.created_at', [$conditions['start_time'],$conditions['end_time']]);
        }else{
            if(isset($conditions['start_time']) && !empty($conditions['start_time'])){
                $data->where('assets.created_at','>',$conditions['start_time']);
            }elseif(isset($conditions['end_time']) && !empty($conditions['end_time'])){
                $data->where('assets.created_at','<',$conditions['end_time']);
            }
        }
        return $data;
    }


    /**
     * Todo:: 获取列表数据
     * @param array $conditions
     * @param array $select
     * @param array $orderArr
     * @param int $page
     * @param int $pageNum
     * @param string $excelType
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Support\Collection
     */
    public function paginateLists($conditions=[],$select=[],$orderArr=[],$page=1,$pageNum=20,$excelType=''){
        $dataWhere = $this->dataWhere($conditions,$select);
        if ($orderArr) {
            foreach ($orderArr as $order) {
                $dataWhere->orderBy($order['field'], $order['direction']);
            }
        } else {
            $dataWhere->orderBy('assets.id', 'desc');
        }
        if($excelType == 'all'){
            // 导出
            $data = $dataWhere->get();
        }else{
            $data = $dataWhere->paginate($pageNum,['*'],'page',$page);
        }
        return $data;
    }


}
