<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Users extends Model
{
    protected $table = 'users';
    public $timestamps = false; // 指示模型是否自动维护时间戳
    protected $dateFormat = 'U'; // 模型日期列的存储格式。
    public $fillable = ['account','password','pwd_salt','nickname','email','mobile_phone','qq','wechat','real_name','id_card','sex','is_certification','status']; //可以注入


    /**
     * Todo:: 状态数组
     * @param int $code
     * @return array|mixed
     */
    public function statusArray($code=-1){
        $data = [
            0 => '禁用',
            1 => '正常',
        ];
        if($code != -1 && isset($data[$code])){
            return $data[$code];
        }
        return $data;
    }

    /**
     * Todo:: 认证数组
     * @param int $code
     * @return array|mixed
     */
    public function certificationArray($code=-1){
        $data = [
            0 => '未认证',
            1 => '认证',
        ];
        if($code != -1 && isset($data[$code])){
            return $data[$code];
        }
        return $data;
    }

    /**
     * Todo:: 性别数组
     * @param int $code
     * @return array|mixed
     */
    public function sexArray($code=-1){
        $data = [
            0 => '保密',
            1 => '男',
            2 => '女',
        ];
        if($code != -1 && isset($data[$code])){
            return $data[$code];
        }
        return $data;
    }

    /**
     * Todo:: 判断名称是否存在
     * @param $account
     * @return mixed
     */
    public function isAccount($account){
        return self::where('account',$account)->value('id');
    }


    /**
     * Todo:: 日志记录判断条件
     * @param array $conditions
     * @param array $select
     * @return \Illuminate\Database\Query\Builder
     */
    public function dataWhere($conditions=[],$select=[]){
        $data = DB::table('users')
            ->leftJoin('users_invite','users_invite.user_id','=','users.id');

        if($select){
            $data->select($select);
        }else{
            $data->select(array(
                'users.*',
                'users_invite.invite_code',
                'users_invite.recommend_id',
            ));
        }
        if(isset($conditions['keyword']) && !empty($conditions['keyword'])){
            $keyword = $conditions['keyword'];
            $data->orWhere(function ($query) use ($keyword) {
                $query->orWhere('users.account', 'like', "%{$keyword}%")
                    ->orWhere('users.id', 'like', "%{$keyword}%")
                    ->orWhere('users.nickname', 'like', "%{$keyword}%")
                    ->orWhere('users.email', 'like', "%{$keyword}%")
                    ->orWhere('users.mobile_phone', 'like', "%{$keyword}%")
                    ->orWhere('users.qq', 'like', "%{$keyword}%")
                    ->orWhere('users.wechat', 'like', "%{$keyword}%")
                    ->orWhere('users.real_name', 'like', "%{$keyword}%")
                    ->orWhere('users.id_card', 'like', "%{$keyword}%")
                    ->orWhere('users_invite.invite_code', 'like', "%{$keyword}%");
            });
        }
        if(isset($conditions['id']) && is_numeric($conditions['id'])){
            $data->where('users.id',$conditions['id']);
        }
        if(isset($conditions['status']) && is_numeric($conditions['status'])){
            $data->where('users.status',$conditions['status']);
        }
        if(isset($conditions['account']) && $conditions['account']){
            $data->where('users.account',$conditions['account']);
        }
        if(isset($conditions['is_certification']) && is_numeric($conditions['is_certification'])){
            $data->where('users.is_certification',$conditions['is_certification']);
        }
        if(isset($conditions['is_del']) && is_numeric($conditions['is_del'])){
            $data->where('users.is_del',$conditions['is_del']);
        }
        if(isset($conditions['recommend_id']) && is_numeric($conditions['recommend_id'])){
            $data->where('users_invite.recommend_id',$conditions['recommend_id']);
        }

        if (isset($conditions['start_time']) && !empty($conditions['start_time']) && isset($conditions['end_time']) && !empty($conditions['end_time'])) {
            $data->whereBetween('users.created_at', [$conditions['start_time'],$conditions['end_time']]);
        }else{
            if(isset($conditions['start_time']) && !empty($conditions['start_time'])){
                $data->where('users.created_at','>',$conditions['start_time']);
            }elseif(isset($conditions['end_time']) && !empty($conditions['end_time'])){
                $data->where('users.created_at','<',$conditions['end_time']);
            }
        }
        return $data;
    }

    /**
     * Todo:: 获取日志列表数据
     * @param array $conditions
     * @param array $select
     * @param array $orderArr
     * @param int $page
     * @param int $pageNum
     * @param string $excelType
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Support\Collection
     */
    public function paginateLists($conditions=[],$select=[],$orderArr=[],$page=1,$pageNum=20,$excelType=''){
        $dataWhere = $this->dataWhere($conditions,$select);
        if ($orderArr) {
            foreach ($orderArr as $order) {
                $dataWhere->orderBy($order['field'], $order['direction']);
            }
        } else {
            $dataWhere->orderBy('users.id', 'desc');
        }
        if($excelType == 'all'){
            // 导出
            $data = $dataWhere->get();
        }else{
            $data = $dataWhere->paginate($pageNum,['*'],'page',$page);
        }
        return $data;
    }


    /**
     * Todo:: 用户信息
     * @param $userId
     * @return Model|\Illuminate\Database\Query\Builder|object|null
     */
    public function userInfo($userId){
        $data = DB::table('users')
            ->join('users_invite','users_invite.user_id','=','users.id')
            ->where('users.id',$userId)
            ->select(array('users.*','users_invite.invite_code','users_invite.recommend_id'))
            ->first();
        $usersAssets = (new UsersAssets())->userAssetsList($userId,'show');
        $data->user_assets = $usersAssets;
        return $data;
    }

    /**
     * Todo:: 软删除
     * @param $id
     * @return mixed
     */
    public function updateDel($id){
        return self::where('id',$id)->update([
            'is_del' => 1,
            'deleted_at' => date('Y-m-d H:i:s')
        ]);
    }
}
