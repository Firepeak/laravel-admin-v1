<?php

namespace App\Providers;

use App\Libs\AdminEloquentUserProvider;
use App\Libs\SessionGuardExtended;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        // 注册新的"记住我"密码
        \Auth::extend(
            'sessionExtended',
            function($app){
                $provider = new AdminEloquentUserProvider($app['hash'],config('auth.providers.admin_user.model'));
                return new SessionGuardExtended('sessionExtended',$provider,app()->make('session.store'));
            }
        );
    }
}
