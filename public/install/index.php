<?php

include 'auto.php';
if(IS_SAE)

if (file_exists('./install.lock')) {
    echo '
		<html>
        <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        </head>
        <body>
        	你已经安装过该系统，如果想重新安装，请先删除站点install目录下的 install.lock 文件，然后再安装。
        </body>
        </html>';
    exit;
}
@set_time_limit(1000);

if (ifPHPVersion('7.1.3'))
    ini_set("magic_quotes_runtime", 0);
if (!ifPHPVersion('7.1.3')){
	header("Content-type:text/html;charset=utf-8");
	exit('您的php版本过低，不能安装本软件，请升级到7.1.3或更高版本再安装，谢谢！');
}

define("Laravel-admin_VERSION", '20200828');
date_default_timezone_set('PRC');
error_reporting(E_ALL & ~E_NOTICE);
header('Content-Type: text/html; charset=UTF-8');
define('SITEDIR', _dir_path(substr(dirname(__FILE__), 0, -8)));

//数据库
$sqlFile = '/sql/mysql_db.sql';     // 数据库sql
$configFile = 'install_env.php';    // 配置数据库文件
if (!file_exists(SITEDIR . 'install/' . $sqlFile) || !file_exists(SITEDIR . 'install/' . $configFile)) {
    echo '缺少必要的安装文件!';
    exit;
}
$Title = "laravel_admin安装向导";
$Powered = "Powered by Mr.Pu";
$steps = array(
    '1' => '安装许可协议',
    '2' => '运行环境检测',
    '3' => '安装参数设置',
    '4' => '安装详细过程',
    '5' => '安装完成',
);
$step = isset($_GET['step']) ? $_GET['step'] : 1;

//地址
$scriptName = !empty($_SERVER["REQUEST_URI"]) ? $scriptName = $_SERVER["REQUEST_URI"] : $scriptName = $_SERVER["PHP_SELF"];
$rootpath = @preg_replace("/\/(I|i)nstall\/index\.php(.*)$/", "", $scriptName);
$domain = empty($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : $_SERVER['SERVER_NAME'];
if ((int) $_SERVER['SERVER_PORT'] != 80) {
    $domain .= ":" . $_SERVER['SERVER_PORT'];
}
$domain = $domain . $rootpath;


switch ($step) {

    case '1':
        include_once ("./templates/step1.php");
        exit();

    case '2':

        if (!ifPHPVersion('7.1.3')) {
            die('本系统需要PHP7+MYSQL >=7.1.3环境，当前PHP版本为：' . phpversion());
        }

        $phpv = @ phpversion();
        $os = PHP_OS;
        //$os = php_uname();
        $tmp = function_exists('gd_info') ? gd_info() : array();
        $server = $_SERVER["SERVER_SOFTWARE"];
        $host = (empty($_SERVER["SERVER_ADDR"]) ? $_SERVER["SERVER_HOST"] : $_SERVER["SERVER_ADDR"]);
        $name = $_SERVER["SERVER_NAME"];
        $max_execution_time = ini_get('max_execution_time');
        $allow_reference = (ini_get('allow_call_time_pass_reference') ? '<font color=green>[√]On</font>' : '<font color=red>[×]Off</font>');
        $allow_url_fopen = (ini_get('allow_url_fopen') ? '<font color=green>[√]On</font>' : '<font color=red>[×]Off</font>');
        $safe_mode = (ini_get('safe_mode') ? '<font color=red>[×]On</font>' : '<font color=green>[√]Off</font>');

        $err = 0;
        if (empty($tmp['GD Version'])) {
            $gd = '<font color=red>[×]Off</font>';
            $err++;
        } else {
            $gd = '<font color=green>[√]On</font> ' . $tmp['GD Version'];
        }
        if (function_exists('mysqli_connect')) {
            $mysql = '<span class="correct_span">&radic;</span> 已安装';
        } else {
            $mysql = '<span class="correct_span error_span">&radic;</span> 请安装mysqli扩展';
            $err++;
        }
        if (ini_get('file_uploads')) {
            $uploadSize = '<span class="correct_span">&radic;</span> ' . ini_get('upload_max_filesize');
        } else {
            $uploadSize = '<span class="correct_span error_span">&radic;</span>禁止上传';
        }
        if (function_exists('session_start')) {
            $session = '<span class="correct_span">&radic;</span> 支持';
        } else {
            $session = '<span class="correct_span error_span">&radic;</span> 不支持';
            $err++;
        }

        if(get_extension_funcs('openssl')){
        	$openssl = '<font color=green>[√]支持</font> ';
        }else{
        	$openssl = '<font color=red>[×]不支持</font>';
        	$err++;
        }
        if(get_extension_funcs('PDO')){
        	$pdo = '<font color=green>[√]支持</font> ';
        }else{
        	$pdo = '<font color=red>[×]不支持</font>';
        	$err++;
        }
        if(get_extension_funcs('mbstring')){
            $mbstring = '<font color=green>[√]支持</font> ';
        }else{
            $mbstring = '<font color=red>[×]不支持</font>';
            $err++;
        }
        if(get_extension_funcs('tokenizer')){
            $tokenizer = '<font color=green>[√]支持</font> ';
        }else{
            $tokenizer = '<font color=red>[×]不支持</font>';
            $err++;
        }
        if(get_extension_funcs('xml')){
            $xml = '<font color=green>[√]支持</font> ';
        }else{
            $xml = '<font color=red>[×]不支持</font>';
            $err++;
        }
        if(get_extension_funcs('ctype')){
            $ctype = '<font color=green>[√]支持</font> ';
        }else{
            $ctype = '<font color=red>[×]不支持</font>';
            $err++;
        }
        if(get_extension_funcs('json')){
            $json = '<font color=green>[√]支持</font> ';
        }else{
            $json = '<font color=red>[×]不支持</font>';
            $err++;
        }

        $folder = array(
            'install',
//            'public/upload',
            '../storage',
//            '../bootstrap/cache ',
        );
        include_once ("./templates/step2.php");
        exit();

    case '3':
		$dbName = strtolower(trim($_POST['dbName']));
		$_POST['dbport'] = $_POST['dbport'] ? $_POST['dbport'] : '3306';
        if ($_GET['testdbpwd']) {
            $dbHost = $_POST['dbHost'];
            $conn = @mysqli_connect($dbHost, $_POST['dbUser'], $_POST['dbPwd'],NULL,$_POST['dbport']);
            if (mysqli_connect_errno($conn)){
				die(json_encode(0));
            } else {
				$result = mysqli_query($conn,"SELECT @@global.sql_mode");
				$result = $result->fetch_array();
				$version = mysqli_get_server_info($conn);
				if ($version >= 5.7)
				{
					if(strstr($result[0],'STRICT_TRANS_TABLES') || strstr($result[0],'STRICT_ALL_TABLES') || strstr($result[0],'TRADITIONAL') || strstr($result[0],'ANSI'))
						exit(json_encode(-1));
				}
				$result = mysqli_query($conn,"select count(table_name) as c from information_schema.`TABLES` where table_schema='$dbName'");
				$result = $result->fetch_array();
				if($result['c'] > 0)
					exit(json_encode(-2));

                exit(json_encode(1));
            }
        }
        include_once ("./templates/step3.php");
        exit();


    case '4':
        if (intval($_GET['install'])) {
            $n = intval($_GET['n']);
            $arr = array();

            $dbHost = trim($_POST['dbhost']);
            $_POST['dbport'] = $_POST['dbport'] ? $_POST['dbport'] : '3306';
            $dbName = strtolower(trim($_POST['dbname']));
            $dbUser = trim($_POST['dbuser']);
            $dbPwd = trim($_POST['dbpw']);
            $dbPrefix = $_POST['dbprefix']?trim($_POST['dbprefix']):'';

            $username = trim($_POST['manager']);
            $password = trim($_POST['manager_pwd']);
            $email	  = trim($_POST['manager_email']);


            if (!function_exists('mysqli_connect')) {
                $arr['msg'] = "请安装 mysqli 扩展!";
                echo json_encode($arr);
                exit;
            }

            $conn = @mysqli_connect($dbHost, $dbUser, $dbPwd,NULL,$_POST['dbport']);
            if (mysqli_connect_errno($conn)){
                $arr['msg'] = "连接数据库失败!".mysqli_connect_error($conn);
                echo json_encode($arr);
                exit;
            }
            mysqli_set_charset($conn, "utf8"); //,character_set_client=binary,sql_mode='';
            $version = mysqli_get_server_info($conn);
            if ($version < 5.5) {
                $arr['msg'] = '数据库版本太低! 必须5.5以上';
                echo json_encode($arr);
                exit;
            }

            if (!mysqli_select_db($conn,$dbName)) {
                //创建数据时同时设置编码
                if (!mysqli_query($conn,"CREATE DATABASE IF NOT EXISTS `" . $dbName . "` DEFAULT CHARACTER SET utf8;")) {
                    $arr['msg'] = '数据库 ' . $dbName . ' 不存在，也没权限创建新的数据库！';
                    echo json_encode($arr);
                    exit;
                }
                if ($n==-1) {
                    $arr['n'] = 0;
                    $arr['msg'] = "成功创建数据库:{$dbName}<br>";
                    echo json_encode($arr);
                    exit;
                }
                mysqli_select_db($conn , $dbName);
            }

            //读取数据文件
            $sqldata = file_get_contents(SITEDIR . 'install/' . $sqlFile);
            $sqlFormat = sql_split($sqldata, $dbPrefix);
            //创建写入sql数据库文件到库中 结束

			mysqli_query($conn,"set global wait_timeout=2147480");
			mysqli_query($conn,"set global interactive_timeout=2147480");
			mysqli_query($conn,"set global max_allowed_packet=104857600");
            /**
             * 执行SQL语句
             */
            $counts = count($sqlFormat);

            for ($i = $n; $i < $counts; $i++) {
                $sql = trim($sqlFormat[$i]);

                if (strstr($sql, 'CREATE TABLE')) {
                    preg_match('/CREATE TABLE `([^ ]*)`/', $sql, $matches);
                    $rowTable = $dbPrefix.$matches[1];
                    mysqli_query($conn,"DROP TABLE IF EXISTS `$rowTable");
                    $sql = str_replace("CREATE TABLE `$matches[1]`","CREATE TABLE `$rowTable`",$sql);
                    $ret = mysqli_query($conn,$sql);
                    if ($ret) {
                        $message = '<li><span class="correct_span">&radic;</span>创建数据表  ' . $rowTable . '  <span style="color: #38ff22;">完成!</span><span style="float: right;">' .date('Y-m-d H:i:s').'</span></li> ';
                    } else {
                        $message = '<li><span class="correct_span error_span">&radic;</span>创建数据表  ' . $rowTable . '  <span style="color: red;">失败!</span><span style="float: right;">'.date('Y-m-d H:i:s').'</span></li>';
                    }
                    $i++;
                    $arr = array('n' => $i, 'msg' => $message);
                    echo json_encode($arr);
                    exit;
                } else {
					if(trim($sql) == '')
					   continue;
                    $ret = mysqli_query($conn,$sql);
                    $message = '';
                    $arr = array('n' => $i, 'msg' => $message);
                    //echo json_encode($arr); exit;
                }
            }

            if ($i == 999999)
                exit;

			// 清空测试数据
			if($_POST['demo'] != 'demo')
			{
				$result = mysqli_query($conn,"show tables");
				$tables = array();
				//$tables=$result->fetch_all(MYSQLI_NUM);//参数MYSQL_ASSOC、MYSQLI_NUM、MYSQLI_BOTH规定产生数组类型
				while($row = mysqli_fetch_array($result)) {
					$tables[] = $row;
				}

				$bl_table = array('admin_menu','config');    // 必须安装的数据库表
				foreach($bl_table as $k => $v)
				{
//					$bl_table[$k] = str_replace('lv_',$dbPrefix,$v);
                    $bl_table[$k] = $dbPrefix.$v;
				}

				foreach($tables as $key => $val)
				{
					if(!in_array($val[0], $bl_table))
					{
						mysqli_query($conn,"truncate table ".$val[0]);
					}
				}
				delFile('../storage/app/public'); // 清空测试图片
			}


            //读取配置文件，并替换真实配置数据1
            $adminKey = sp_random_string(9);
            $strConfig = file_get_contents(SITEDIR . 'install/' . $configFile);
            $strConfig = str_replace('#ADMIN_KEY#', $adminKey, $strConfig);     // 后台登录加密 key
            $strConfig = str_replace('#DB_HOST#', $dbHost, $strConfig);     // 服务器地址
            $strConfig = str_replace('#DB_NAME#', $dbName, $strConfig);     // 数据库名
            $strConfig = str_replace('#DB_USER#', $dbUser, $strConfig);     // 用户
            $strConfig = str_replace('#DB_PWD#', $dbPwd, $strConfig);   // 密码
            $strConfig = str_replace('#DB_PORT#', $_POST['dbport'], $strConfig);    // 端口
            $strConfig = str_replace('#DB_PREFIX#', $dbPrefix, $strConfig);     // 数据表前缀
            $strConfig = str_replace('#DB_CHARSET#', 'utf8', $strConfig);   // 数据库编码默认采用utf8
            $strConfig = str_replace('#DB_COLLATION#', 'utf8_general_ci', $strConfig);   // 数据库编码默认采用utf8_general_ci排序
            $strConfig = str_replace('#DB_DEBUG#', 'false', $strConfig);      //
            $envAddress = SITEDIR . '../.env';
            if (!file_exists($envAddress)) {       // 判断文件夹 不存在者创建
                mkdir($envAddress, 0755, true);
            }
            @chmod($envAddress,0777); //数据库配置文件的地址
            @file_put_contents($envAddress, $strConfig); //数据库配置文件的地址

            //插入管理员表字段admin_user表
            $ip = get_client_ip();
            $ip = empty($ip) ? "0.0.0.0" : $ip;
            $salt = 'laravel_admin666';
            $password = md5($password.$salt);

            $tableName = "admin_user";
            if($dbPrefix){
                $tableName = "{$dbPrefix}admin_user";
            }

            mysqli_query($conn,"delete from `{$tableName}` where username = '".$username."'");
            $query = mysqli_query($conn,"insert into `{$tableName}` (`id`,`username`,`password`,`salt`,`email`,`status`,`is_super`,`reg_ip`) values ('1','$username','$password','$salt','$email',1,1,'$ip')");
            if ($query) {
                $message = '<li><span class="correct_span">&radic;</span>成功添加管理员  <span style="color: #38ff22;">完成!</span><span style="float: right;">'.date('Y-m-d H:i:s').'</span></li> ';
            } else {
                $message = '<li><span class="correct_span error_span">&radic;</span>添加管理员 <span style="color: red;">失败!</span><span style="float: right;">'.date('Y-m-d H:i:s').'</span></li>';
            }
            $n = 999999;
            $conn->close(); // 关闭数据库链接
			$message .= '成功写入配置文件<br>安装完成．';
            $arr = array('n' => $n, 'msg' => $message,'data'=>array('username'=>$username));
            echo json_encode($arr);
            exit;
        }

        include_once ("./templates/step4.php");
        exit();

    case '5':
        /*
        $ip = get_client_ip();
        $host = $_SERVER['HTTP_HOST'];
        */
        $curent_version = file_get_contents(SITEDIR .'../config/version.txt');
        $create_date = date("Ymdhis");
        $time = time();
        $mt_rand_str = $create_date.sp_random_string(6);
        $str_constant = "<?php".PHP_EOL."return [
    'install_date' =>".$time.",
    'SERIALNUMBER' => '".$mt_rand_str."',
];";
        @file_put_contents(SITEDIR . '../config/constant.php', $str_constant);

        $phpExecInit = phpExecInit();

        $username = trim($_GET['username']);
        $adminKey = ObtainEnv('ADMIN_KEY');

        $token = adminLoginToken($username,$adminKey);
        $adminLoginUrl = "/admin?username={$username}&token={$token}";

        include_once ("./templates/step5.php");
        @touch('./install.lock');
        exit();

}

/**
 * Todo:: 初始化化 项目 清理缓存、软两链接、重新生成key
 * @return array
 */
function phpExecInit(){
    unset($output);
    exec("cd ../../ && php artisan config:cache && php artisan view:clear && php artisan cache:clear ; php artisan key:generate ; php artisan storage:link",$output,$status);
    return array(
        'status' => $status,
        'output' => $output,
    );
}

/**
 * Todo:: 获取 .env 配置文件
 * @param string $name
 * @return mixed|string
 */
function ObtainEnv($name='ADMIN_KEY'){
    $env = file_get_contents(SITEDIR . '../.env');
    $array = explode("\n",$env);
    $string = "";
    foreach($array as $key=>$val){
        if(preg_match("/{$name}/i",$val)){
            $string = $val;
            break;
        }
    }
    if($string){
        $strArr = explode("=",$string);
        if($strArr){
            return trim($strArr[1]);
        }
    }
    return '';
}

/**
 * Todo:: 后台登录token
 * @param $username
 * @param $key
 * @return string
 */
function adminLoginToken($username,$key){
    return md5(sha1($username.$key));
}


function testwrite($d) {
    $tfile = "_test.txt";
    $fp = @fopen($d . "/" . $tfile, "w");
    if (!$fp) {
        return false;
    }
    fclose($fp);
    $rs = @unlink($d . "/" . $tfile);
    if ($rs) {
        return true;
    }
    return false;
}

function sql_execute($sql, $tablepre) {
    $sqls = sql_split($sql, $tablepre);
    if (is_array($sqls)) {
        foreach ($sqls as $sql) {
            if (trim($sql) != '') {
                mysqli_query($sql);
            }
        }
    } else {
        mysqli_query($sqls);
    }
    return true;
}

function sql_split($sql, $tablepre) {

    if ($tablepre != "lv_")
    	$sql = str_replace("lv_", $tablepre, $sql);

    $sql = preg_replace("/TYPE=(InnoDB|MyISAM|MEMORY)( DEFAULT CHARSET=[^; ]+)?/", "ENGINE=\\1 DEFAULT CHARSET=utf8", $sql);

    $sql = str_replace("\r", "\n", $sql);
    $ret = array();
    $num = 0;
    $queriesarray = explode(";\n", trim($sql));
    unset($sql);
    foreach ($queriesarray as $query) {
        $ret[$num] = '';
        $queries = explode("\n", trim($query));
        $queries = array_filter($queries);
        foreach ($queries as $query) {
            $str1 = substr($query, 0, 1);
            if ($str1 != '#' && $str1 != '-')
                $ret[$num] .= $query;
        }
        $num++;
    }
    return $ret;
}

function _dir_path($path) {
    $path = str_replace('\\', '/', $path);
    if (substr($path, -1) != '/')
        $path = $path . '/';
    return $path;
}

// 获取客户端IP地址
function get_client_ip() {
    static $ip = NULL;
    if ($ip !== NULL)
        return $ip;
    if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $arr = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
        $pos = array_search('unknown', $arr);
        if (false !== $pos)
            unset($arr[$pos]);
        $ip = trim($arr[0]);
    }elseif (isset($_SERVER['HTTP_CLIENT_IP'])) {
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (isset($_SERVER['REMOTE_ADDR'])) {
        $ip = $_SERVER['REMOTE_ADDR'];
    }
    // IP地址合法验证
    $ip = (false !== ip2long($ip)) ? $ip : '0.0.0.0';
    return $ip;
}

function dir_create($path, $mode = 0777) {
    if (is_dir($path))
        return TRUE;
    $ftp_enable = 0;
    $path = dir_path($path);
    $temp = explode('/', $path);
    $cur_dir = '';
    $max = count($temp) - 1;
    for ($i = 0; $i < $max; $i++) {
        $cur_dir .= $temp[$i] . '/';
        if (@is_dir($cur_dir))
            continue;
        @mkdir($cur_dir, 0777, true);
        @chmod($cur_dir, 0777);
    }
    return is_dir($path);
}

function dir_path($path) {
    $path = str_replace('\\', '/', $path);
    if (substr($path, -1) != '/')
        $path = $path . '/';
    return $path;
}

function sp_password($pw, $pre){
	$decor = md5($pre);
	$mi = md5($pw);
	return substr($decor,0,12).$mi.substr($decor,-4,4);
}

function sp_random_string($len = 8) {
	$chars = array(
			"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k",
			"l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v",
			"w", "x", "y", "z", "A", "B", "C", "D", "E", "F", "G",
			"H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R",
			"S", "T", "U", "V", "W", "X", "Y", "Z", "0", "1", "2",
			"3", "4", "5", "6", "7", "8", "9"
	);
	$charsLen = count($chars) - 1;
	shuffle($chars);    // 将数组打乱
	$output = "";
	for ($i = 0; $i < $len; $i++) {
		$output .= $chars[mt_rand(0, $charsLen)];
	}
	return $output;
}
// 递归删除文件夹
function delFile($dir,$file_type='') {
	if(is_dir($dir)){
		$files = scandir($dir);
		//打开目录 //列出目录中的所有文件并去掉 . 和 ..
		foreach($files as $filename){
			if($filename!='.' && $filename!='..'){
				if(!is_dir($dir.'/'.$filename)){
					if(empty($file_type)){
						unlink($dir.'/'.$filename);
					}else{
						if(is_array($file_type)){
							//正则匹配指定文件
							if(preg_match($file_type[0],$filename)){
								unlink($dir.'/'.$filename);
							}
						}else{
							//指定包含某些字符串的文件
							if(false!=stristr($filename,$file_type)){
								unlink($dir.'/'.$filename);
							}
						}
					}
				}else{
					delFile($dir.'/'.$filename);
					rmdir($dir.'/'.$filename);
				}
			}
		}
	}else{
		if(file_exists($dir)) unlink($dir);
	}
}

function ifPHPVersion($claim='7.1.13'){
    $current =  phpversion();
    $currentArr = explode('.',$current);
    $claimArr = explode('.',$claim);
    if($currentArr[0] < $claimArr[0]){
        return false;
    }elseif ($currentArr[0] == $claimArr[0] && $currentArr[1] < $claimArr[1]){
        return false;
    }elseif($currentArr[0] == $claimArr[0] && $currentArr[1] == $claimArr[1] && $currentArr[2] < $claimArr[2]){
        return false;
    }else{
        return true;
    }
}

?>
