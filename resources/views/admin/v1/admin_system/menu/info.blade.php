@extends("admin.v1.common.main")
@section("content")

    <div class="page-content">


        <div class="page-header">
            <h1>
                <span>详情</span>
                <div style="width: 50%; font-size: 0.7rem;display: inline-block;">
                    <a href="/admin"><span>主页</span></a>
                    @foreach($menu_parent as $items)
                        <a href="{{$items['path_url']}}"><span>/{{$items['menu_name']}}</span></a>
                    @endforeach
                </div>
                <button class="btn btn-sm btn-primary pull-right" onclick="javascript:window.location.href = 'lists'">
                    返回列表</button>
            </h1>

        </div><!-- /.page-header -->

        <div class="row">
            <div class="col-xs-12">
                <!-- PAGE CONTENT BEGINS -->
                <form class="form-horizontal" role="form" method="POST" action="{{isset($info->id)?'edit':'add'}}">
                    {{csrf_field()}}
                    @if(isset($info->id))
                        <input type="hidden" name="id" value="{{$info->id}}"/>
                    @endif
                    @include("admin.v1.common.error")
                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> 上级菜单 </label>
                        <div class="col-sm-9">
                            {{From::select($menus,request('parent_id')?request('parent_id'):(isset($info->id)?$info->parent_id:0),' name="parent_id" class="col-xs-10 col-sm-8" ','--作为一级菜单--')}}
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right">图标</label>
                        <div class="col-sm-9">
                        <span class="input-icon input-icon-left">
                            <input type="text" id="form-field-icon-2" name="icon" value="{{$info->icon}}">
                            <i class="ace-icon fa fa-leaf green"></i>
                        </span>
                            <!-- <a href="http://fontawesome.io/cheatsheet/" target="_blank">查看</a>-->
                            <a href="#modal-form" role="button" class="blue" data-toggle="modal"> 查看图标 </a>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right"> 名称 </label>
                        <div class="col-sm-9">
                            <input type="text" name="menu_name" value="{{$info->menu_name}}"
                                   class="col-xs-10 col-sm-8">
                        </div>
                    </div>
                    <div class="space-4"></div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right"> 模版 </label>
                        <div class="col-sm-9">
                            <input type="text" name="m" value="{{$info->m}}" class="col-xs-10 col-sm-8">
                        </div>
                    </div>

                    <div class="space-4"></div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right"> 版本 </label>
                        <div class="col-sm-9">
                            <input type="text" name="v" value="{{$info->v}}" class="col-xs-10 col-sm-8">
                        </div>
                    </div>

                    <div class="space-4"></div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right"> 地址 </label>
                        <div class="col-sm-9">
                            <input type="text" name="address" value="{{$info->address}}" class="col-xs-10 col-sm-8">
                        </div>
                    </div>
                    <div class="space-4"></div>




                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right"> 附加参数 </label>
                        <div class="col-sm-9">
                            <input type="text" name="data" value="{{$info->data}}" class="col-xs-10 col-sm-8">
                        </div>
                    </div>
                    <div class="space-4"></div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right"> 排序 </label>
                        <div class="col-sm-9">
                            <input type="text" name="list_order" value="{{$info->list_order}}"
                                   class="col-xs-10 col-sm-8">
                        </div>
                    </div>
                    <div class="space-4"></div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right"> 菜单类型 </label>
                        <div class="col-sm-9">
                            {!! From::radio($menuType,isset($info->is_type)?$info->is_type:1,' name="is_type" ',70,'is_display') !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right"> 菜单状态 </label>
                        <div class="col-sm-9">
                            {!! From::radio($menuDisplay,isset($info->is_display)?$info->is_display:1,' name="is_display" ',70,'is_display') !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right"> 记录日志 </label>
                        <div class="col-sm-9">
                            {!! From::radio($menuWriteLog,isset($info->write_log)?$info->write_log:0,' name="write_log" ',70,'write_log') !!}
                        </div>
                    </div>
                </form>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div>
@endsection

