@extends("admin.v1.common.main")
@section("content")

<div class="page-content">
    <div class="page-header">
        <h1>
            {{$menu_info->menu_name}}
            @if(isPermission('admin_user/group/create',$_v))
				<span class="btn btn-sm btn-primary pull-right"
					  onclick="javascript:window.location.href = 'create'">创建</span>
			@endif
        </h1>
    </div>

    <div class="operate panel panel-default">
        <div class="panel-body ">
            <form name="myform" method="GET" class="form-inline">
                <div class="form-group select-input">
                    <div class="input-group">
                        <div class="input-group-addon">时间</div>
                        <input type="text" class="layui-input" id="start_time"  name="start_time"  value="{{request('start_time')}}">
                    </div>

                    <div class="input-group" style="margin-left: 0;">
                        <div class="input-group-addon"> 至</div>
                        <input type="text" class="layui-input" id="end_time"  name="end_time" value="{{request('end_time')}}">
                    </div>

                    <div class="input-group">
                        <div class="input-group-addon">状态</div>
                        {{From::select($statusArray,request('status'),'class="form-control" name="status"','--请选择--')}}
                    </div>

                    <div class="input-group">
                        <div class="input-group-addon">角色名称</div>
                        <input class="form-control" name="name" type="text" value="{{request('name')}}">
                    </div>

                    <div class="input-group" style="float: right;">
                        <input type="submit" value="搜索" class="btn btn-danger btn-sm">
                        <span class="btn btn-info btn-sm" onclick="window.location.href = '?'">重置</span>
                    </div>
                </div>

            </form>
        </div>
    </div>

    <div class="row">
	<div class="col-xs-12">
	    <!-- PAGE CONTENT BEGINS -->
	    <div class="row">
		<div class="col-xs-12">
		    <table id="simple-table" class="table  table-bordered table-hover">
			<thead>
			    <tr>
				<th>id</th>
				<th>角色名称</th>
				<th>描述</th>
				<th>排序</th>
				<th>状态</th>
				<th>绑定数量</th>
				<th>更新者</th>
				<th>创建时间</th>
				<th>修改时间</th>
				<th>操作</th>
			    </tr>
			</thead>

			<tbody>
            @if(isset($lists))
			    @foreach ($lists as $info)
			    <tr>
				<td>{{$info->id}}</td>
				<td>{{$info->name}}</td>
				<td>{{$info->description}}</td>
				<td>{{$info->list_order}}</td>
				<td>
                    @if(isset($statusArray[$info->status]))
                        {{ $statusArray[$info->status] }}
                    @endif
                </td>
				<td>{{ $info->admin_num }}</td>
				<td>{{$info->username}}</td>
				<td>{{$info->created_at}}</td>
				<td>{{$info->updated_at}}</td>
				<td>
				    <div class="hidden-sm hidden-xs btn-group">
						@if(isPermission('admin_user/group/info',$_v))
							<a href="info?id={{$info->id}}">详情</a>
						@endif
						@if(isPermission('admin_user/group/edit',$_v))
							<a href="edit?id={{$info->id}}">编辑</a>
						@endif
						@if(isPermission('admin_user/group/del',$_v))
                                <a class="lists_tr_del" data-id="{{$info->id}}" data-token="{{csrf_token()}}" title="删除">删除</a>
{{--							<a href="del?id={{$info->id}}">删除</a>--}}
						@endif
				    </div>
				</td>
			    </tr>
			    @endforeach
            @endif
			</tbody>
		    </table>
            @if(isset($lists))
                <div id="page">
                    {{$lists->appends(request()->all())->links()}}
                    <div style="float: right;margin: 20px 0;">共<strong style="color: red;margin: 0 5px;">{{$lists->total()}}</strong>条</div>
                </div>
            @endif
		</div><!-- /.span -->

	    </div><!-- /.row -->
	    <!-- PAGE CONTENT ENDS -->
	</div><!-- /.col -->
    </div>
 </div>

    <script>
        $(function(){
            $(".lists_tr_del").on('click',function(){
                var _id = $(this).data('id');
                var _token = $(this).data('token');
                var _this = this;


                layer.confirm('您确定要删除？', {
                    btn: ['确定','取消'], //按钮
                    icon: 0,
                }, function(){
                    var index = layer.load(0, {shade: false});
                    $.ajax({
                        url:"/admin/v1/admin_user/group/del",
                        type:"delete",
                        dataType:"json",
                        data:{id:_id,_token:_token},
                        success:function (response) {
                            layer.close(index);
                            if(response.status && response.code=='000000'){
                                $(_this).parents('tr').remove();
                                layer.msg('删除成功', {icon: 1});
                            }else{
                                layer.msg(response.message, {
                                    icon: 2,
                                    // time: 20000, //20s后自动关闭
                                    btn: ['知道了']
                                });
                            }
                        },
                    })

                }, function(){
                    layer.close(layer.index);
                });
            });
        });
    </script>

    @endsection
