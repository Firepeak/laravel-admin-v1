@extends("admin.v1.common.main")
@section("content")

    <style>
        .table tbody td{
            overflow:hidden;
            /*white-space:nowrap;*/
            overflow-x: scroll;
        }
    </style>

    <div class="page-content">


        <div class="page-header">
            <h1>
                <span>详情</span>
                <div style="width: 50%; font-size: 0.7rem;display: inline-block;">
                    <a href="/admin"><span>主页</span></a>
                    @foreach($menu_parent as $items)
                        <a href="{{$items['path_url']}}"><span>/{{$items['menu_name']}}</span></a>
                    @endforeach
                </div>
                <button class="btn btn-sm btn-primary pull-right" onclick="javascript:window.location.href = 'lists'">
                    返回列表</button>
            </h1>

        </div><!-- /.page-header -->

        <div class="row">
            <div class="col-xs-12">

                <div class="col-xs-12 col-sm-6 widget-container-col ui-sortable" style="min-height: 263px;">
                    <div class="widget-box widget-color-blue ui-sortable-handle" style="opacity: 1;">
                        @if(isset($info->id))
                            <div class="widget-header">
                                <h5 class="widget-title bigger lighter">
                                    <i class="ace-icon fa fa-table"></i>
                                    本次操作记录({{$info->id}})
                                </h5>
                            </div>

                            <div class="widget-body">
                                <div class="widget-main no-padding">
                                    <table class="table table-striped table-bordered table-hover">
                                        <tbody>
                                        <tr>
                                            <td class="">菜单名</td>
                                            <td>
                                                {{$info->menu_name}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="">操作人</td>
                                            <td>
                                                {{$info->admin_name}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="">真实姓名</td>
                                            <td>
                                                <span>{{$info->realname}}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="">时间</td>
                                            <td>
                                                {{$info->created_at}}
                                            </td>
                                        </tr>

                                        <tr>
                                            <td class="">地点</td>
                                            <td>
                                                {{$info->ip}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="">请求方式</td>
                                            <td>
                                                {{$info->method}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="">请求地址</td>
                                            <td style="min-width:400px;max-width: 500px">
                                                {{$info->m}}/{{$info->v}}/{{$info->address}}{{!empty($info->query_string)?'?'.$info->query_string:''}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="">POST数据</td>
                                            <td>
                                                @if(isset($info->data))
                                                    {!! arr2str($info->data) !!}
                                                @endif
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>


                <div class="col-xs-12 col-sm-6 widget-container-col ui-sortable" style="min-height: 263px;">
                    <div class="widget-box widget-color-grey ui-sortable-handle" style="opacity: 1;">
                        <div class="widget-header">
                            <h5 class="widget-title bigger lighter">
                                <i class="ace-icon fa fa-table"></i>
                                原始记录
                            </h5>
                        </div>

                        <div class="widget-body">
                            <div class="widget-main no-padding">
                                <table class="table table-striped table-bordered table-hover">
                                    <tbody>

                                    @if(isset($last_info->id))
                                        <tr>
                                            <td class="">菜单名</td>
                                            <td>
                                                {{$last_info->menu_name}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="">操作人</td>
                                            <td>
                                                {{$last_info->admin_name}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="">真实姓名</td>
                                            <td>
                                                <span>{{$last_info->realname}}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="">时间</td>
                                            <td>
                                                {{$last_info->created_at}}
                                            </td>
                                        </tr>

                                        <tr>
                                            <td class="">地点</td>
                                            <td>
                                                {{$last_info->ip}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="">请求方式</td>
                                            <td>
                                                {{$last_info->method}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="">请求地址</td>
                                            <td style="min-width:400px;max-width: 500px">
                                                {{$last_info->m}}/{{$last_info->v}}/{{$last_info->address}}{{!empty($last_info->query_string)?'?'.$last_info->query_string:''}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="">POST数据</td>
                                            <td>
                                                @if($last_info->data)
                                                    {!! arr2str($last_info->data) !!}
                                                @endif
                                            </td>
                                        </tr>

                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>



            </div><!-- /.col -->
        </div><!-- /.row -->
    </div>
@endsection

