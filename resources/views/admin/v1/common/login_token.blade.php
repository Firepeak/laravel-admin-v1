<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Generate background login link</title>
</head>
<body>
<div style="margin: 50px auto 0;box-shadow: 0 0 10px #ccc;width: 750px;background: #fff;">
    <div style="text-align: center;line-height: 4rem;font-size: 2rem;background-color: #10a2ff;color: #ffffff;">
        Generate background login link
    </div>
    <div style="padding: 2rem;line-height: 3rem;overflow: hidden;">
        <form name="form" method="post" action="">
            <div>
                <label
                    style="width: 30%;text-align: right;color: #8f9598;font-size: 1.5rem;float: left;">Username</label>
                <div style="width: 69%;float: left;">
                    <input type="text" name="username" value="{{ request('username') }}" placeholder="Login username" required
                           style="line-height: 1.5rem;margin-left: 0.5rem;width: 20rem;">
                </div>
            </div>
            <div>
                <label style="width: 30%;text-align: right;color: #8f9598;font-size: 1.5rem;float: left;">Key</label>
                <div style="width: 69%;float: left;">
                    <input type="text" name="key" value="{{ request('key') }}" placeholder="Background login key" required
                           style="line-height: 1.5rem;margin-left: 0.5rem;width: 20rem;">
                </div>
            </div>
            <div>
                <div style="text-align: center;">
                    <button style="padding: 0.2rem 0.5rem;" id="submit">Submit</button>
                </div>
            </div>

            @if(isset($token))
                <div style="border: 1px solid #8f9598;overflow: hidden;">
                    <div style="background-color: #ffe4bc;line-height: 2.5rem;font-size: 1.2rem;text-align: center;">
                        Response
                    </div>
                    <div style="line-height: 1.5rem;overflow: hidden;margin: 1rem;">
                        <div style="width: 100%;overflow: hidden;">
                            <label style="width: 6rem;text-align: right;color: #8f9598;float: left;">Username:</label>
                            <div style="float: left;">
                                <div style="margin-left: 0.5rem;">{{ request('username') }}</div>
                            </div>
                        </div>

                        <div style="width: 100%;overflow: hidden;">
                            <label style="width: 6rem;text-align: right;color: #8f9598;float: left;">Token:</label>
                            <div style="float: left;">
                                <div style="margin-left: 0.5rem;">{{ isset($token)?$token:"" }}</div>
                            </div>
                        </div>

                        <div style="width: 100%;overflow: hidden;">
                            <label style="width: 6rem;text-align: right;color: #8f9598;float: left;">Url:</label>
                            <div style="float: left;width: 33rem;height: auto;overflow: hidden;">
                                <div style="margin-left: 0.5rem;">
                                    <a
                                        href="{{ isset($loginUrl)?$loginUrl:"#" }}" style="word-wrap:break-word;">{{ isset($loginUrl)?$loginUrl:"" }}</a>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            @endif

        </form>
    </div>
</div>
</body>
</html>
