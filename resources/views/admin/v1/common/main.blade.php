<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>
        @if (isset($backstage_set->backstage_name)){{ $backstage_set->backstage_name }}@endif
        @if (isset($menu_info->menu_name))-{{ $menu_info->menu_name }}@endif
    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0"/>
    <link rel="shortcut icon" href="@if (isset($backstage_set->backstage_icon_url)){{ $backstage_set->backstage_icon_url }}@endif" type="image/x-icon"/>
    <!-- bootstrap & fontawesome -->
    <link rel="stylesheet" href="/assets/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="/assets/font-awesome/4.5.0/css/font-awesome.min.css"/>

    <!-- page specific plugin styles -->
    <link rel="stylesheet" href="/assets/css/bootstrap-duallistbox.min.css"/>
    <link rel="stylesheet" href="/assets/css/bootstrap-multiselect.min.css"/>
    <link rel="stylesheet" href="/assets/css/select2.min.css"/>
    <link rel="stylesheet" href="/assets/bt-select/css/bootstrap-select.min.css"/>
    <!-- text fonts -->
    <link rel="stylesheet" href="/assets/css/fonts.googleapis.com.css"/>
    <!-- ace styles -->
    <link rel="stylesheet" href="/assets/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style"/>

    <!--[if lte IE 9]>
    <link rel="stylesheet" href="/assets/css/ace-part2.min.css" class="ace-main-stylesheet"/>
    <![endif]-->
    <link rel="stylesheet" href="/assets/css/ace-skins.min.css"/>
    <link rel="stylesheet" href="/assets/css/ace-rtl.min.css"/>

    <!--[if lte IE 9]>
    <link rel="stylesheet" href="/assets/css/ace-ie.min.css"/>
    <![endif]-->

    <!-- inline styles related to this page -->

    <!-- ace settings handler -->
    <script src="/assets/js/ace-extra.min.js"></script>
    <!-- HTML5shiv and Respond.js')}} for IE8 to support HTML5 elements and media queries -->
    <!--[if lte IE 8]>
    <script src="/assets/js/html5shiv.min.js"></script>
    <script src="/assets/js/respond.min.js"></script>
    <![endif]-->

    <!-- basic scripts -->
    <!--[if !IE]> -->
    <script src="/assets/js/jquery-2.1.4.min.js"></script>
    <!-- <![endif]-->
    <!--[if IE]>
    <script src="/assets/js/jquery-1.11.3.min.js"></script>
    <![endif]-->
    <script type="text/javascript">
        if ('ontouchstart' in document.documentElement)
            document.write("<script src='/assets/js/jquery.mobile.custom.min.js'>" + "<" + "/script>");
    </script>
    <script src="/assets/js/bootstrap.min.js"></script>

    <!-- page specific plugin scripts -->

    <script src="/assets/js/jquery.bootstrap-duallistbox.min.js"></script>
    <script src="/assets/js/jquery.raty.min.js"></script>
    <script src="/assets/js/bootstrap-multiselect.min.js"></script>
    <script src="/assets/js/select2.min.js"></script>
    <script src="/assets/js/jquery-typeahead.js"></script>
    <script src="/assets/js/laydate/laydate.js"></script>
    <script src="/assets/js/tree.min.js"></script>
    <script src="/assets/js/dropzone.min.js"></script>
    <script src="/assets/bt-select/js/bootstrap-select.min.js"></script>


    <!-- ace scripts -->
    <script src="/assets/js/ace-elements.min.js"></script>
    <script src="/assets/js/ace.min.js"></script>


    <script src="/assets/fileinput/js/plugins/sortable.js" type="text/javascript"></script>
    <link href="/assets/fileinput/css/fileinput.css" media="all" rel="stylesheet" type="text/css"/>
    <link href="/assets/fileinput/themes/explorer/theme.css" media="all" rel="stylesheet" type="text/css"/>
    <script src="/assets/fileinput/js/fileinput.js" type="text/javascript"></script>
    <script src="/assets/fileinput/js/locales/zh.js" type="text/javascript"></script>
    <script src="/assets/fileinput/themes/explorer/theme.js" type="text/javascript"></script>

    <!-- -->
    <link rel="stylesheet" href="/css/admin/style.css?v=20200808"/>
    <!-- layer -->
    <script src="/layer/pop-ups/layer.js"></script>
    <!-- iconfont -->
    <link rel="stylesheet" href="/iconfont/iconfont.css?v=20200819"/>

    <style>
        .select-input {
            font-size: 0;
        }

        .select-input .input-group {
            margin-top: 10px;
            margin-left: 10px;
        }
    </style>
</head>
<body class="no-skin">

<div id="navbar" class="navbar navbar-default  ace-save-state">
    <div class="navbar-container ace-save-state" id="navbar-container">
        <div class="navbar-header pull-left" style="width:190px">
            <button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" data-target="#sidebar">
                <span class="sr-only">Toggle sidebar</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <span class="navbar-brand">
                <small>
                   <i class="fa  fa-home" title="首页" onclick="window.location.href='/';"></i>
                   <span onclick="window.location.href='/admin';"> @if (isset($backstage_set->backstage_name)){{ $backstage_set->backstage_name }}@endif</span>
                </small>
            </span>
        </div>

        <div class="navbar-buttons navbar-header pull-right hidden-sm hidden-xs" role="navigation">
            <ul class="nav ace-nav">
                <li class="grey dropdown-modal">
                    <span style="color: #ffffff;margin-right: 1rem;">@if(isset($current_date)){{ $current_date }}@endif</span>
                </li>
                <li class="light-blue dropdown-modal">
                    <a data-toggle="dropdown" href="#" class="dropdown-toggle">
                        <img class="nav-user-photo" src="@if(isset($admin_user->head_portrait)){{ imageUrl($admin_user->head_portrait) }}@else /assets/images/avatars/user.jpg @endif" alt="Photo" />
                        <span class="user-info">
				    <small>Welcome,</small>
                            @if (isset($admin_user->username)){{ $admin_user->username }}@endif
				</span>

                        <i class="ace-icon fa fa-caret-down"></i>
                    </a>

                    <ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">

                        <li>
                            <a href="/admin/v1/admin_user/index/my_edit?id={{ $admin_user->id }}">
                                <i class="ace-icon fa fa-user"></i>
                                更新资料
                            </a>
                        </li>

                        <li class="divider"></li>
                        <li>
                            <a href="#" class="my_pwd" data-id="{{ $admin_user->id }}">
                                <i class="ace-icon fa fa-cog"></i>
                                更新密码
                            </a>
                        </li>
                        @if(isPermission($_address,$_v))
                        <li class="divider"></li>
                        <li>
                            <a href="/admin/v1/system/config/clean_cache">
                                <i class="ace-icon fa  fa-filter"></i>
                                清理缓存
                            </a>
                        </li>
                        @endif

                        <li class="divider"></li>

                        <li>
                            <a href="/admin/v1/admin_user/logout">
                                <i class="ace-icon fa fa-power-off"></i>
                                退出系统
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>

    </div><!-- /.navbar-container -->
</div>

<div class="main-container ace-save-state" id="main-container">
    <script type="text/javascript">
        try {
            ace.settings.loadState('main-container')
        } catch (e) {
        }
    </script>

    <div id="sidebar" class="sidebar  responsive   ace-save-state">
        <script type="text/javascript">
            try {
                ace.settings.loadState('sidebar')
            } catch (e) {
            }
        </script>


        <!-- /.nav-list -->
        <ul class="nav nav-list">
            {{--{!!m('Menu')->myMenuHtml()!!}--}}
            {!!$my_menu_html!!}
        </ul>

        <!-- /.nav-list -->

        <div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
            <i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state"
               data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
        </div>
    </div>

    <div class="main-content">
        <div class="main-content-inner">


            <!-- /.page-content -->
        @yield("content")
        <!-- /.page-content -->


        </div>

    </div><!-- /.main-content -->


    <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
        <i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
    </a>
</div>
<!-- /.main-container -->


<script>
    var u = $(".active").parent('ul');

    var uc = u.attr("class"); //

    if (uc == 'submenu') {
        u.parent().attr("class", "open active");
        if (u.parent().parent().attr('class') == 'submenu') {
            u.parent().parent().parent().attr("class", "open active");
        }
    }
    function show(id) {
        $("#" + id).siblings().attr('class', 'hide');
        $("#" + id).attr('class', 'open');
    }

    $(function () {
        laydate.render({
            elem: '#start_time',
            calendar: true,
            type: 'datetime'
        });
        laydate.render({
            elem: '#end_time',
            calendar: true,
            type: 'datetime'

        });
        laydate.render({
            elem: '#date',
            calendar: true,
            type: 'date'
        });
    })

    // 调整登录
    function login(){
        window.location.href = "/admin";
    }
    // 当 AJAX 请求失败时，触发一个警告框：
    $(document).ajaxError(function(event,xhr,options,exc){
        console.log(xhr);
        layer.close(layer.index);
        if(xhr.status == 401) {
            var responseJSON = xhr.responseJSON;
            var message = responseJSON.message;
            var code = responseJSON.code;
            if (code == '100000') {
                layer.confirm('登录过期请重新登录!', {
                    btn: ['确定'] //按钮
                }, function(){
                    window.location.href = "/admin";
                });
            }
        }else if(xhr.status == 422){
            let responseError = xhr.responseJSON.errors;
            $.each(responseError, function (index, item) {
                let html = "";
                $.each(item, function (key, val) {
                    html += "<p>提示: "+val+"</p>";
                });
                $("input[name="+index+"]").parents(".warn-div").find(".warn-span").html(html);
            })
        }else if(xhr.status != 200){
            layer.alert('请求错误',{
                icon: 2,
                title:'提示'
            });
        }
    });

    /**
     * Todo:: 隐藏列表操作下拉框
     */
    $(document).mouseup(function (e) {
        var _con = $('.list-more-actions,.more-actions');   // 设置目标区域
        if (!_con.is(e.target) && _con.has(e.target).length === 0) { // Mark 1
            listMoreActionsHide();
        }
    });
    function listMoreActionsHide(){
        $(".list-more-actions").each(function () {
            if ($(this).hasClass("more-show")) {
                $(this).removeClass("more-show");
                if($(".more-actions").find('.ace-icon').hasClass('fa-chevron-down')){
                    $(".more-actions").find('.ace-icon').removeClass('fa-chevron-down');
                    $(".more-actions").find('.ace-icon').addClass('fa-chevron-up');
                }

            }
            if (!$(this).hasClass("more-hide")) {
                $(this).addClass("more-hide");
                if($(".more-actions").find('.ace-icon').hasClass('fa-chevron-up')){
                    $(".more-actions").find('.ace-icon').removeClass('fa-chevron-up');
                    $(".more-actions").find('.ace-icon').addClass('fa-chevron-down');
                }
            }
        });
    }
    /**
     * Todo:: 更多操作 点击显示、隐藏
     */
    $(".more-actions").on('click', function () {
        var list_more = $(this).parents('td').find('.list-more-actions');
        if(list_more.hasClass("more-show")){
            list_more.removeClass('more-show');
            list_more.addClass('more-hide');
            if($(this).find('.ace-icon').hasClass('fa-chevron-up')){
                $(this).find('.ace-icon').removeClass('fa-chevron-up');
                $(this).find('.ace-icon').addClass('fa-chevron-down');
            }
        }else{
            // 显示 <i class="ace-icon fa fa-chevron-up"></i>
            listMoreActionsHide();
            list_more.removeClass('more-hide');
            list_more.addClass('more-show');
            console.log($(this).find('.ace-icon').hasClass('fa-chevron-down'));
            if($(this).find('.ace-icon').hasClass('fa-chevron-down')){
                $(this).find('.ace-icon').removeClass('fa-chevron-down');
                $(this).find('.ace-icon').addClass('fa-chevron-up');
            }
        }
    });

    $(".my_pwd").on('click',function(){
        var _id = $(this).data('id');
        layer.open({
            type: 2,
            title: '修改密码',
            shadeClose: true,
            shade: 0.8,
            area: ['380px', '350px'],
            content: '/admin/v1/admin_user/index/my_pwd?id='+_id //iframe的url
        });
    });


    $("img").one("error", function(e){
        $(this).attr("src", "/assets/images/avatars/user.jpg");
    });


</script>
</body>
</html>
