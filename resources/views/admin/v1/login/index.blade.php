<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <meta charset="utf-8"/>
    <title>@if (isset($backstage_set->backstage_name)){{ $backstage_set->backstage_name }}@else后台管理系统@endif</title>
    <meta name="description" content="overview &amp; stats"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0"/>
    <link rel="shortcut icon" href="@if (isset($backstage_set->backstage_icon_url)){{ $backstage_set->backstage_icon_url }}@endif" type="image/x-icon"/>
    <!-- bootstrap & fontawesome -->
    <link rel="stylesheet" href="/assets/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="/assets/css/daterangepicker.min.css"/>
    <link rel="stylesheet" href="/assets/font-awesome/4.5.0/css/font-awesome.min.css"/>

    <!-- page specific plugin styles -->

    <!-- text fonts -->
    <link rel="stylesheet" href="/assets/css/fonts.googleapis.com.css"/>

    <!-- ace styles -->
    <link rel="stylesheet" href="/assets/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style"/>

    <!--[if lte IE 9]>
    <link rel="stylesheet" href="/assets/css/ace-part2.min.css" class="ace-main-stylesheet"/>
    <![endif]-->
    <link rel="stylesheet" href="/assets/css/ace-skins.min.css"/>
    <link rel="stylesheet" href="/assets/css/ace-rtl.min.css"/>

    <!--[if lte IE 9]>
    <link rel="stylesheet" href="/assets/css/ace-ie.min.css"/>
    <![endif]-->

    <!-- inline styles related to this page -->

    <!-- ace settings handler -->
    <script src="/assets/js/ace-extra.min.js"></script>
    <!-- HTML5shiv and Respond.js')}} for IE8 to support HTML5 elements and media queries -->
    <!--[if lte IE 8]>
    <script src="/assets/js/html5shiv.min.js"></script>
    <script src="/assets/js/respond.min.js"></script>
    <![endif]-->

    <!-- basic scripts -->
    <!--[if !IE]> -->
    <script src="/assets/js/jquery-2.1.4.min.js"></script>
    <!-- <![endif]-->
    <!--[if IE]>
    <script src="/assets/js/jquery-1.11.3.min.js"></script>
    <![endif]-->
    <script type="text/javascript">
        if ('ontouchstart' in document.documentElement)
            document.write("<script src='/assets/js/jquery.mobile.custom.min.js')}}'>" + "<" + "/script>");
    </script>
    <script src="/assets/js/bootstrap.min.js"></script>
    <!-- page specific plugin scripts -->
    <!--[if lte IE 8]>
    <script src="/assets/js/excanvas.min.js"></script>
    <![endif]-->
    <script src="/assets/js/jquery-ui.custom.min.js"></script>
    <script src="/assets/js/jquery.ui.touch-punch.min.js"></script>
    <script src="/assets/js/jquery.easypiechart.min.js"></script>
    <script src="/assets/js/jquery.sparkline.index.min.js"></script>
    <script src="/assets/js/jquery.flot.min.js"></script>
    <script src="/assets/js/jquery.flot.pie.min.js"></script>
    <script src="/assets/js/jquery.flot.resize.min.js"></script>
    <script src="/assets/js/laydate/laydate.js"></script>

    <!-- ace scripts -->
    <script src="/assets/js/ace-elements.min.js"></script>
    <script src="/assets/js/ace.min.js"></script>
    <!-- inline scripts related to this page -->

    <!-- -->
    <link rel="stylesheet" href="/css/admin/style.css?v=20200808"/>
    <!-- layer -->
    <script src="/layer/pop-ups/layer.js"></script>
    <style>
        .select-input {
            font-size: 0;
        }

        .select-input .input-group {
            margin-top: 10px;
            margin-left: 10px;
        }


    </style>
</head>
<body class="login-layout">
<div class="main-container">
    <div class="main-content">
        <div class="row">
            <div class="col-sm-10 col-sm-offset-1">
                <div class="login-container">
                    <div class="center ">
                        <h1>
                            <i class="ace-icon fa fa-leaf green"></i>
                            <span class="red">@if (isset($backstage_set->backstage_name)){{ $backstage_set->backstage_name }}@else后台管理系统@endif</span>
                            <span class="white" id="id-text2"></span>
                        </h1>
                        <h4 class="blue" id="id-company-text"></h4>
                    </div>

                    <div class="space-6"></div>

                    <div class="position-relative">
                        <div id="login-box" class="login-box visible widget-box no-border">
                            <div class="widget-body">
                                <div class="widget-main">
                                    <h4 class="header blue lighter bigger" title="请输入您的信息">
                                        <i class="ace-icon fa fa-coffee green"></i>
                                        Please Enter Your Information
                                    </h4>

                                    <div class="space-6"></div>

                                    <form method="POST" name="myform" action="{{ url('admin/v1/admin_user/login') }}">
                                        {{csrf_field()}}
                                        <fieldset>
                                            <label class="block clearfix warn-div">
                                            <span class="block input-icon input-icon-right">
                                                <input type="text" class="form-control" name="username" value="" placeholder="username" title="用户名"/>
                                                <i class="ace-icon fa fa-user"></i>
                                            </span>
                                                <div class="warn-span col-xs-10 col-sm-8"></div>
                                             </label>

                                            <label class="block clearfix warn-div">
                                            <span class="block input-icon input-icon-right">
                                                <input type="password" class="form-control" name="password" value="" placeholder="password" title="密码"/>
                                                <i class="ace-icon fa fa-lock"></i>
                                            </span>
                                                <div class="warn-span col-xs-10 col-sm-8"></div>
                                            </label>

                                            <div class="space"></div>

                                            <div class="clearfix">
                                                <label class="inline">
                                                    <input type="checkbox" class="ace" name="is_remember" value="1"/>
                                                    <span class="lbl" title="记住我"> Remember Me</span>
                                                </label>
                                                <input type="button" name="dosubmit" value="Login" class="width-35 pull-right btn btn-sm btn-primary form-submit">

                                            </div>

                                            <div class="space-4"></div>
                                        </fieldset>
                                    </form>


                                </div><!-- /.widget-main -->

                                <div class="toolbar clearfix" style="height: 3rem;text-align: center;line-height: 3rem;">
                                        <a href="#" class="user-signup-link" title="不要在公众场合记住密码">
                                            Don't remember your password in public.
                                        </a>
                                </div>
                                @include('admin.v1.common.error')
                            </div><!-- /.widget-body -->
                        </div><!-- /.login-box -->

                    </div><!-- /.position-relative -->

                    <div class="navbar-fixed-top align-right hidden-sm hidden-xs">
                        <br/>
                        &nbsp;
                        <a id="btn-login-dark" href="#">Dark</a>
                        &nbsp;
                        <span class="blue">/</span>
                        &nbsp;
                        <a id="btn-login-blur" href="#">Blur</a>
                        &nbsp;
                        <span class="blue">/</span>
                        &nbsp;
                        <a id="btn-login-light" href="#">Light</a>
                        &nbsp; &nbsp; &nbsp;
                    </div>
                </div>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.main-content -->
</div><!-- /.main-container -->

<!-- basic scripts -->

<!--[if !IE]> -->
<script src="/assets/js/jquery-2.1.4.min.js"></script>

<!-- <![endif]-->

<!--[if IE]>
<script src="/assets/js/jquery-1.11.3.min.js"></script>
<![endif]-->
<script type="text/javascript">
    if ('ontouchstart' in document.documentElement)
        document.write("<script src='assets/js/jquery.mobile.custom.min.js'>" + "<" + "/script>");
</script>

<!-- inline scripts related to this page -->
<script type="text/javascript">
    jQuery(function ($) {
        $(document).on('click', '.toolbar a[data-target]', function (e) {
            e.preventDefault();
            var target = $(this).data('target');
            $('.widget-box.visible').removeClass('visible');//hide others
            $(target).addClass('visible');//show target
        });
    });

    // Get the parameters in the link
    function getQueryVariable(variable)
    {
        var query = window.location.search.substring(1);
        var vars = query.split("&");
        for (var i=0;i<vars.length;i++) {
            var pair = vars[i].split("=");
            if(pair[0] == variable){return pair[1];}
        }
        return(false);
    }

    // login
    function loginFun(){
        let formData = $("form[name=myform]").serializeArray();
        let data = {};
        let _status = true;

        $.each(formData, function (index, item) {
            data[item.name] = $.trim(item.value);
            $("input[name="+item.name+"]").parents(".warn-div").find(".warn-span").html("");
            if(item.name == 'username' && (item.value == null || item.value == '') ){
                $("input[name="+item.name+"]").parents(".warn-div").find(".warn-span").html("<p>提示: 账户不能为空</p>");
                _status = false;
            }
            if(item.name == 'password' && (item.value == null || item.value == '') ){
                $("input[name="+item.name+"]").parents(".warn-div").find(".warn-span").html("<p>提示: 密码不能为空</p>");
                _status = false;
            }
        });
        // token
        data['token'] = getQueryVariable('token');

        if(_status && data){
            $.ajax({
                url:"/admin/v1/admin_user/login",
                type:"post",
                dataType:"json",
                data:data,
                success:function (response) {
                    if(response.status && response.code=='000000'){
                        var _data = response.data;
                        layer.msg(response.message, {icon: 1,time:2000},function(){
                            window.location.href = _data.url;
                        });
                    }else{
                        layer.msg(response.message, {
                            icon: 2,
                            // time: 20000, //20s后自动关闭
                            btn: ['知道了']
                        });
                    }
                },
                error:function(jqXHR, textStatus, errorThrown){
                    console.log(jqXHR);
                    if(jqXHR.status == 422 && textStatus == 'error'){
                        let responseError = jqXHR.responseJSON.errors;
                        $.each(responseError, function (index, item) {
                            let html = "";
                            $.each(item, function (key, val) {
                                html += "<p>提示: "+val+"</p>";
                            });
                            $("input[name="+index+"]").parents(".warn-div").find(".warn-span").html(html);
                        })
                    }else if(jqXHR.status != 200){
                        layer.msg('请求错误', {
                            icon: 2,
                            // time: 20000, //20s后自动关闭
                            btn: ['知道了']
                        });

                    }
                }
            });
        }
    }

    //you don't need this, just used for changing background
    jQuery(function ($) {
        $('#btn-login-dark').on('click', function (e) {
            $('body').attr('class', 'login-layout');
            $('#id-text2').attr('class', 'white');
            $('#id-company-text').attr('class', 'blue');

            e.preventDefault();
        });
        $('#btn-login-light').on('click', function (e) {
            $('body').attr('class', 'login-layout light-login');
            $('#id-text2').attr('class', 'grey');
            $('#id-company-text').attr('class', 'blue');

            e.preventDefault();
        });
        $('#btn-login-blur').on('click', function (e) {
            $('body').attr('class', 'login-layout blur-login');
            $('#id-text2').attr('class', 'white');
            $('#id-company-text').attr('class', 'light-blue');

            e.preventDefault();
        });
        // login
        $(".form-submit").on('click',function(){
            let formData = $("form[name=myform]").serializeArray();
            let data = {};
            let _status = true;

            $.each(formData, function (index, item) {
                data[item.name] = $.trim(item.value);
                $("input[name="+item.name+"]").parents(".warn-div").find(".warn-span").html("");
                if(item.name == 'username' && (item.value == null || item.value == '') ){
                    $("input[name="+item.name+"]").parents(".warn-div").find(".warn-span").html("<p>提示: 账户不能为空</p>");
                    _status = false;
                }
                if(item.name == 'password' && (item.value == null || item.value == '') ){
                    $("input[name="+item.name+"]").parents(".warn-div").find(".warn-span").html("<p>提示: 密码不能为空</p>");
                    _status = false;
                }
            });
            // token
            data['token'] = getQueryVariable('token');

            if(_status && data){
                $.ajax({
                    url:"/admin/v1/admin_user/login",
                    type:"post",
                    dataType:"json",
                    data:data,
                    success:function (response) {
                        if(response.status && response.code=='000000'){
                            var _data = response.data;
                            layer.msg(response.message, {icon: 1,time:2000},function(){
                                window.location.href = _data.url;
                            });
                        }else{
                            layer.msg(response.message, {
                                icon: 2,
                                // time: 20000, //20s后自动关闭
                                btn: ['知道了']
                            });
                        }
                    },
                    error:function(jqXHR, textStatus, errorThrown){
                        console.log(jqXHR);
                        if(jqXHR.status == 422 && textStatus == 'error'){
                            let responseError = jqXHR.responseJSON.errors;
                            $.each(responseError, function (index, item) {
                                let html = "";
                                $.each(item, function (key, val) {
                                    html += "<p>提示: "+val+"</p>";
                                });
                                $("input[name="+index+"]").parents(".warn-div").find(".warn-span").html(html);
                            })
                        }else if(jqXHR.status != 200){
                            layer.msg('请求错误', {
                                icon: 2,
                                // time: 20000, //20s后自动关闭
                                btn: ['知道了']
                            });

                        }
                    }
                });
            }
        })
        $(document).keyup(function(event){
            if(event.keyCode ==13){
                //这里填写你要做的事件
                loginFun();
            }
        });

    });


</script>
</body>
</html>
