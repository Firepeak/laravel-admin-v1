@extends("admin.v1.common.main")
@section("content")

    <div class="page-content">


        <div class="page-header">
            <h1>
                <span>创建</span>
                <div style="width: 50%; font-size: 0.7rem;display: inline-block;">
                    <a href="/admin"><span>主页</span></a>
                    @foreach($menu_parent as $items)
                        <a href="{{$items['path_url']}}"><span>/{{$items['menu_name']}}</span></a>
                    @endforeach
                </div>
                <button class="btn btn-sm btn-primary pull-right" onclick="javascript:window.location.href = 'lists'">
                    返回列表
                </button>
            </h1>

        </div><!-- /.page-header -->

        <div class="row">
            <div class="col-xs-12">

            @include("admin.v1.common.error")
            <!-- PAGE CONTENT BEGINS -->
                <form id="form" name="myform" class="form-horizontal" role="form" method="POST" action="create" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="form-group warn-div">
                        <label class="col-sm-3 control-label no-padding-right"><code>*</code> 账户 </label>
                        <div class="col-sm-9 warn-div">
                            <input type="text" name="account" value="" class="col-xs-10 col-sm-8" minlength="2" maxlength="20" placeholder="格式:长度2～20"
                                   autocomplete="off" required>
                            <div class="warn-span col-xs-10 col-sm-8"></div>
                        </div>
                    </div>

                    <div class="form-group warn-div">
                        <label class="col-sm-3 control-label no-padding-right"><code>*</code> 密码 </label>
                        <div class="col-sm-9 warn-div">
                            <input type="password" name="password" value="" class="col-xs-10 col-sm-8" minlength="6" maxlength="18" placeholder="格式:长度6～18"
                                   autocomplete="off" required>
                            <div class="warn-span col-xs-10 col-sm-8"></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right"> 平台名称 </label>
                        <div class="col-sm-9 warn-div">
                            <input type="text" name="name" value="" class="col-xs-10 col-sm-8" minlength="2" maxlength="20" placeholder="格式:长度2～20"
                                   autocomplete="off">
                            <div class="warn-span col-xs-10 col-sm-8"></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right"> 平台IP </label>
                        <div class="col-sm-9 warn-div">
                            <input type="text" name="ip" value="" class="col-xs-10 col-sm-8" maxlength="20" placeholder="格式:最大长度30"
                                   autocomplete="off">
                            <div class="warn-span col-xs-10 col-sm-8"></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right"> 访问域名 </label>
                        <div class="col-sm-9 warn-div">
                            <input type="text" name="domain_name" value="" class="col-xs-10 col-sm-8" maxlength="40" placeholder="格式:最大长度50"
                                   autocomplete="off">
                            <div class="warn-span col-xs-10 col-sm-8"></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right"><code>*</code> 签名 </label>
                        <div class="col-sm-4 warn-div">
                            <input type="text" name="sign_key" value="" class="col-xs-10 col-sm-12" maxlength="100" placeholder="格式:最大长度100"
                                   autocomplete="off">
                            <div class="warn-span col-xs-10 col-sm-8"></div>
                        </div>
                        <div class="col-sm-5">
                            <button class="btn btn-info btn-sm generate-key" data-type="sign_key" data-number="12" type="button">
                                生成AES签名
                            </button>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right"><code>*</code> AES签名 </label>
                        <div class="col-sm-4 warn-div">
                            <input type="text" name="aes_key" value="" class="col-xs-10 col-sm-12" maxlength="100" placeholder="格式:最大长度100"
                                   autocomplete="off">
                            <div class="warn-span col-xs-10 col-sm-8"></div>
                        </div>
                        <div class="col-sm-5">
                            <button class="btn btn-info btn-sm generate-key" data-type="aes_key" data-number="32" type="button">
                                生成AES签名
                            </button>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right"><code>*</code> AES向量 </label>
                        <div class="col-sm-4 warn-div">
                            <input type="text" name="aes_vector" value="" class="col-xs-10 col-sm-12" maxlength="16" placeholder="格式:长度16"
                                   autocomplete="off">
                            <div class="warn-span col-xs-10 col-sm-8"></div>
                        </div>
                        <div class="col-sm-5">
                            <button class="btn btn-info btn-sm generate-key" data-type="aes_vector" data-number="16" type="button">
                                生成AES签名
                            </button>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right">状态 </label>
                        <div class="col-sm-9">
                            {!! From::radio($statusArray,isset($info->status)?$info->status:1,' name="status" ',70,'status') !!}
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right"> 简介 </label>
                        <div class="col-sm-9">
                            <textarea name="introduction" class="col-xs-10 col-sm-8" rows="2" cols="20" maxlength="200" style="height:150px;"></textarea>
                        </div>
                    </div>


                    <div class="clearfix form-actions">
                        <div class="col-md-offset-3 col-md-9">
                            <button class="btn btn-info form-submit" type="button" id="dosubmit">
                                <i class="ace-icon fa fa-check bigger-110"></i>
                                提交
                            </button>
                            <button class="btn reset" type="reset">
                                <i class="ace-icon fa fa-undo bigger-110"></i>
                                Reset
                            </button>
                        </div>
                    </div>
                </form>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div>
    @include('admin.v1.common.img')

    <script>
        $(function(){
            $(".generate-key").on('click',function(){
                var input_name = $(this).data('type');
                var number = $(this).data('number');
                $.ajax({
                    url: "/admin/v1/common/index/generate_key",
                    type: "post",
                    dataType: "json",
                    data: {number:number},
                    success: function (response) {
                        if(response.status && response.code=='000000'){
                            var _data = response.data;
                            $("input[name="+input_name+"]").val(_data);
                        }else{
                            layer.msg(response.message, {
                                icon: 2,
                                // time: 20000, //20s后自动关闭
                                btn: ['知道了']
                            });
                        }
                    }
                });
            });

            $(".form-submit").on('click',function(){
                let data = {};
                let value = $('#form').serializeArray();
                let _status = true;
                let _names = [];
                $.each(value, function (index, item) {
                    data[item.name] = $.trim(item.value);
                    $("input[name="+item.name+"]").parent("div").find(".warn-span").html("");
                    if(item.name == 'account'){
                        var html = "";
                        // var reg=/^[\w\d\u4e00-\u9fff,]{2,20}$/;
                        var reg=/^[A-Za-z]{1}[A-Za-z0-9_-]{2,20}$/;
                        if((item.value == null || item.value == '')){
                            html = "<p>提示: 账户不能为空</p>";
                        }else if(!reg.test(item.value)){
                            html = "<p>提示: 格式错误;必须是以字母开头，只能包含字母数字下划线和减号，2到20位.</p>";
                        }
                        if(html != ""){
                            $("input[name="+item.name+"]").parent("div").find(".warn-span").html(html);
                            _status = false;
                        }
                    }
                    if(item.name == 'password'){
                        var html = "";
                        // var reg=/^[\w\d\u4e00-\u9fff,]{6,18}$/;
                        // var reg=/(?=^.{6,18}$)(?=.*\d)(?=.*[A-Z])(?=.*[a-z])(?=.*[!@#$%^&*]).*$/;
                        var reg=/(?=^.{6,18}$)(?=.*\d)(?=.*[A-Z])(?=.*[a-z]).*$/;
                        if((item.value == null || item.value == '')){
                            html = "<p>提示: 密码不能为空</p>";
                        }else if(!reg.test(item.value)){
                            // html = "<p>提示: 6-16位，,至少有一个数字，一个大写字母，一个小写字母和一个特殊字符，四个任意组合.</p>";
                            html = "<p>提示: 6-18位，至少有一个数字，一个大写字母，一个小写字母，三个任意组合.</p>";
                        }
                        if(html != ""){
                            $("input[name="+item.name+"]").parent("div").find(".warn-span").html(html);
                            _status = false;
                        }
                    }

                    if(item.name == 'sign_key'){
                        var html = "";
                        // var reg=/^[\w\d\u4e00-\u9fff,]{6,18}$/;
                        // var reg=/(?=^.{6,18}$)(?=.*\d)(?=.*[A-Z])(?=.*[a-z])(?=.*[!@#$%^&*]).*$/;
                        var reg=/^[\w\d\u4e00-\u9fff,]{12,100}$/;
                        if((item.value == null || item.value == '')){
                            html = "<p>提示: 签名不能为空</p>";
                        }else if(!reg.test(item.value)){
                            html = "<p>提示: 12-100位之间.</p>";
                        }
                        if(html != ""){
                            $("input[name="+item.name+"]").parent("div").find(".warn-span").html(html);
                            _status = false;
                        }
                    }

                    if(item.name == 'aes_key'){
                        var html = "";
                        var reg=/^[\w\d\u4e00-\u9fff,]{32,100}$/;
                        if((item.value == null || item.value == '')){
                            html = "<p>提示: AES密钥不能为空</p>";
                        }else if(!reg.test(item.value)){
                            html = "<p>提示: 32-100位之间.</p>";
                        }
                        if(html != ""){
                            $("input[name="+item.name+"]").parent("div").find(".warn-span").html(html);
                            _status = false;
                        }
                    }

                    if(item.name == 'aes_vector'){
                        var html = "";
                        var reg=/^[\w\d\u4e00-\u9fff,]{16}$/;
                        if((item.value == null || item.value == '')){
                            html = "<p>提示: AES向量不能为空</p>";
                        }else if(!reg.test(item.value)){
                            html = "<p>提示: 16位字符串.</p>";
                        }
                        if(html != ""){
                            $("input[name="+item.name+"]").parent("div").find(".warn-span").html(html);
                            _status = false;
                        }
                    }


                });

                if(_status){
                    // var json = JSON.stringify(data);
                    // console.log(json);
                    var index = layer.load(0, {shade: false});
                    $.ajax({
                        url:"/admin/v1/platform/index/create",
                        type:"post",
                        dataType:"json",
                        data:data,
                        success:function (response) {
                            layer.close(index);
                            if(response.status && response.code=='000000'){
                                var _data = response.data;
                                layer.msg(response.message, {icon: 1},function(){
                                    window.location.href = _data.url;
                                });

                            }else{
                                layer.alert(response.message,{
                                    icon: 2,
                                    title:'提示'
                                });
                            }
                        },
                        error:function(jqXHR, textStatus, errorThrown){
                            layer.close(layer.index);
                            if(jqXHR.status == 422 && textStatus == 'error'){
                                let responseError = jqXHR.responseJSON.errors;
                                $.each(responseError, function (index, item) {
                                    let html = "";
                                    $.each(item, function (key, val) {
                                        html += "<p>提示: "+val+"</p>";
                                    });
                                    $("input[name="+index+"]").parent("div").find(".warn-span").html(html);
                                })
                            }else if(jqXHR.status != 200){
                                layer.msg('请求错误', {
                                    icon: 2,
                                    // time: 20000, //20s后自动关闭
                                    btn: ['知道了']
                                });
                            }
                        }
                    });

                }
            });
            $(".reset").on('click',function(){
                $(".warn-span").html("");
            });
        });
    </script>
@endsection

