@extends("admin.v1.common.main")
@section("content")

<div class="page-content">
    <div class="page-header">
        <h1>
            {{$menu_info->menu_name}}
            @if(isPermission('platform/index/create',$_v))
				<span class="btn btn-sm btn-primary pull-right"
					  onclick="javascript:window.location.href = 'create'">创建</span>
			@endif
        </h1>
    </div>

    <div class="operate panel panel-default">
        <div class="panel-body ">
            <form name="myform" method="GET" class="form-inline">
                <div class="form-group select-input">
                    <div class="input-group">
                        <div class="input-group-addon">时间</div>
                        <input type="text" class="layui-input" id="start_time"  name="start_time"  value="{{request('start_time')}}" autocomplete="off">
                    </div>

                    <div class="input-group" style="margin-left: 0;">
                        <div class="input-group-addon"> 至</div>
                        <input type="text" class="layui-input" id="end_time"  name="end_time" value="{{request('end_time')}}" autocomplete="off">
                    </div>

                    <div class="input-group">
                        <div class="input-group-addon">状态</div>
                        {{From::select($statusArray,request('status'),'class="form-control" name="status"','--请选择--')}}
                    </div>

                    <div class="input-group">
                        <div class="input-group-addon">关键字</div>
                        <input class="form-control" name="keyword" type="text" value="{{request('keyword')}}" placeholder="账户/平台名称/IP/域名" autocomplete="off">
                    </div>

                    <div class="input-group" style="float: right;">
                        <input type="submit" value="搜索" class="btn btn-danger btn-sm">
                        <span class="btn btn-info btn-sm" onclick="window.location.href = '?'">重置</span>
                    </div>
                </div>

            </form>
        </div>
    </div>

    <div class="row">
	<div class="col-xs-12">
	    <!-- PAGE CONTENT BEGINS -->
	    <div class="row">
		<div class="col-xs-12">
		    <table id="simple-table" class="table  table-bordered table-hover">
			<thead>
			    <tr>
				<th>id</th>
				<th>账户</th>
				<th>平台名称</th>
				<th>平台IP</th>
                <th>访问域名</th>
				<th>状态</th>
				<th>创建时间</th>
				<th>修改时间</th>
				<th>操作</th>
			    </tr>
			</thead>

			<tbody>
            @if(isset($lists))
			    @foreach ($lists as $info)
			    <tr>
				<td>{{$info->id}}</td>
				<td>{{$info->account}}</td>
				<td>{{$info->name}}</td>
				<td>{{$info->ip}}</td>
				<td>{{$info->domain_name}}</td>
                    <td>
                        @if(isPermission('platform/index/edit',$_v))
                            @if($info->status==1)
                                <button class="btn btn-success btn-minier list-tr-status" onclick="listTrStatus(this);" data-status="0" data-id="{{$info->id}}" data-username="{{$info->account}}" data-token="{{csrf_token()}}" title="点击禁用">启用
                                </button>
                            @else
                                <button class="btn btn-minier list-tr-status" onclick="listTrStatus(this);" data-status="1" data-id="{{$info->id}}" data-username="{{$info->account}}" data-token="{{csrf_token()}}" title="点击启用">禁用
                                </button>
                            @endif
                        @else
                            @if($info->status==1)
                                <button class="btn btn-success btn-minier">启用
                                </button>
                            @else
                                <button class="btn btn-minier" >禁用
                                </button>
                            @endif
                        @endif

                    </td>

				<td>{{$info->created_at}}</td>
				<td>{{$info->updated_at}}</td>
				<td>

				    <div class="hidden-sm hidden-xs btn-group">
                        <button class="more-actions">更多操作&nbsp;<i class="ace-icon fa fa-chevron-down"></i></button>
                        <div class="list-more-actions more-hide">
                            <ul>
                                @if(isPermission('platform/index/info',$_v))
                                    <a href="info?id={{$info->id}}">
                                        <li>详情</li>
                                    </a>
                                @endif
                                @if(isPermission('platform/index/edit',$_v))
                                    <a href="edit?id={{$info->id}}">
                                        <li>编辑</li>
                                    </a>
                                @endif
                                @if(isPermission('platform/index/del',$_v))
                                    <a href="#" class="lists_tr_del" data-id="{{$info->id}}" data-token="{{csrf_token()}}">
                                        <li>删除</li>
                                    </a>
                                @endif
                            </ul>
                        </div>

				    </div>
				</td>
			    </tr>
			    @endforeach
            @endif
			</tbody>
		    </table>
            @if(isset($lists))
                <div id="page">
                    {{$lists->appends(request()->all())->links()}}
                    <div style="float: right;margin: 20px 0;">共<strong style="color: red;margin: 0 5px;">{{$lists->total()}}</strong>条</div>
                </div>
            @endif
		</div><!-- /.span -->

	    </div><!-- /.row -->
	    <!-- PAGE CONTENT ENDS -->
	</div><!-- /.col -->
    </div>
 </div>

<script>
    $(function () {
        /**
         * Todo:: 删除
         */
        $(".lists_tr_del").on('click', function () {
            var _id = $(this).data('id');
            var _token = $(this).data('token');
            var _this = this;

            layer.confirm('您确定要删除？', {
                btn: ['确定', '取消'], //按钮
                icon: 0,
            }, function () {
                var index = layer.load(0, {shade: false});
                $.ajax({
                    url: "/admin/v1/platform/index/del",
                    type: "delete",
                    dataType: "json",
                    data: {id: _id, _token: _token},
                    success: function (response) {
                        layer.close(index);
                        if (response.status && response.code == '000000') {
                            $(_this).parents('tr').remove();
                            layer.msg('删除成功', {icon: 1});
                        } else {
                            layer.msg(response.message, {
                                icon: 2,
                                // time: 20000, //20s后自动关闭
                                btn: ['知道了']
                            });
                        }
                    },
                })

            }, function () {
                layer.close(layer.index);
            });
        });

        $(".lists_tr_pwd").on('click',function(){
            listMoreActionsHide();
            var _id = $(this).data('id');
            layer.open({
                type: 2,
                title: '修改密码',
                shadeClose: true,
                shade: 0.8,
                area: ['380px', '350px'],
                content: 'pwd?id='+_id //iframe的url
            });
        });

    });

    /**
     * Todo:: 改变状态
     * @param obj
     */
    function listTrStatus(obj){
        var _this = obj;
        var _id = $(_this).data('id');
        var _status = $(_this).data('status');
        var _username = $(_this).data('username');
        var _token = $(_this).data('token');
        var prompt = '禁用';
        var _td_html = '<button class="btn btn-minier btn-minier list-tr-status" onclick="listTrStatus(this);" data-status="1" data-id="'+_id+'" data-username="'+_username+'" data-token="{{csrf_token()}}" title="点击启用">禁用</button>';
        if(_status == '1'){
            prompt = '启用';
            _td_html = '<button class="btn btn-success btn-minier list-tr-status" onclick="listTrStatus(this);" data-status="0" data-id="'+_id+'" data-username="'+_username+'" data-token="{{csrf_token()}}" title="点击禁用">启用</button>';
        }
        layer.confirm('您确定'+prompt+'账户 "'+_username+'" ?',{
            btn: ['确定', '取消'], //按钮
            icon: 0,
        },function(){
            var index = layer.load(0, {shade: false});
            $.ajax({
                url: "/admin/v1/platform/index/edit",
                type: "post",
                dataType: "json",
                data: {id: _id, _token: _token,status:_status,_edit_type:'status'},
                success: function (response) {
                    layer.close(index);
                    if (response.status && response.code == '000000') {
                        $(_this).parent('td').html(_td_html);
                        layer.msg(response.message, {icon: 1});
                    } else {
                        layer.msg(response.message, {
                            icon: 2,
                            // time: 20000, //20s后自动关闭
                            btn: ['知道了']
                        });
                    }
                },
            });

        },function(){
            layer.close(layer.index);
        });
    }

</script>

    @endsection
