@extends("admin.v1.common.main")
@section("content")

    <div class="page-content">


        <div class="page-header">
            <h1>
                <span>编辑</span>
                <div style="width: 50%; font-size: 0.7rem;display: inline-block;">
                    <a href="/admin"><span>主页</span></a>
                    @foreach($menu_parent as $items)
                        <a href="{{$items['path_url']}}"><span>/{{$items['menu_name']}}</span></a>
                    @endforeach
                </div>
                <button class="btn btn-sm btn-primary pull-right" onclick="javascript:window.location.href = 'lists'">
                    返回列表
                </button>
            </h1>

        </div><!-- /.page-header -->

        <div class="row">
            <div class="col-xs-12">

            @include("admin.v1.common.error")
            <!-- PAGE CONTENT BEGINS -->
                <form id="form" name="myform" class="form-horizontal" role="form" method="POST" action="create" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <input type="hidden" name="id" value="{{ $info->id }}">
                    <div class="form-group warn-div">
                        <label class="col-sm-3 control-label no-padding-right"><code>*</code> 账户 </label>
                        <div class="col-sm-9 warn-div">
                            <input type="text" name="account" value="{{ $info->account }}" class="col-xs-10 col-sm-8" minlength="2" maxlength="20" placeholder="格式:长度2～20"
                                   autocomplete="off" disabled="disabled">
                            <div class="warn-span col-xs-10 col-sm-8"></div>
                        </div>
                    </div>

                    <div class="form-group warn-div">
                        <label class="col-sm-3 control-label no-padding-right"> 密码 </label>
                        <div class="col-sm-9 warn-div">
                            <input type="password" name="password" value="" class="col-xs-10 col-sm-8" minlength="6" maxlength="18" placeholder="不修改为空即可"
                                   autocomplete="off" required>
                            <div class="warn-span col-xs-10 col-sm-8"></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right"> 昵称 </label>
                        <div class="col-sm-9 warn-div">
                            <input type="text" name="nickname" value="{{ $info->nickname }}" class="col-xs-10 col-sm-8" minlength="2" maxlength="20" placeholder="格式:长度2～20"
                                   autocomplete="off">
                            <div class="warn-span col-xs-10 col-sm-8"></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right"> 邮箱 </label>
                        <div class="col-sm-9 warn-div">
                            <input type="text" name="email" value="{{ $info->email }}" class="col-xs-10 col-sm-8" maxlength="30" placeholder="格式:最大长度30"
                                   autocomplete="off">
                            <div class="warn-span col-xs-10 col-sm-8"></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right"> 手机号码 </label>
                        <div class="col-sm-9 warn-div">
                            <input type="text" name="mobile_phone" value="{{ $info->mobile_phone }}" class="col-xs-10 col-sm-8" maxlength="20" placeholder="格式:最大长度20"
                                   autocomplete="off">
                            <div class="warn-span col-xs-10 col-sm-8"></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right"> QQ </label>
                        <div class="col-sm-9 warn-div">
                            <input type="text" name="qq" value="{{ $info->qq }}" class="col-xs-10 col-sm-8" maxlength="20" placeholder="格式:最大长度20"
                                   autocomplete="off">
                            <div class="warn-span col-xs-10 col-sm-8"></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right"> 微信 </label>
                        <div class="col-sm-9 warn-div">
                            <input type="text" name="wechat" value="{{ $info->wechat }}" class="col-xs-10 col-sm-8" maxlength="20" placeholder="格式:最大长度20"
                                   autocomplete="off">
                            <div class="warn-span col-xs-10 col-sm-8"></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right"> 真实姓名 </label>
                        <div class="col-sm-9 warn-div">
                            <input type="text" name="real_name" value="{{ $info->real_name }}" class="col-xs-10 col-sm-8" maxlength="20" placeholder="格式:最大长度20"
                                   autocomplete="off">
                            <div class="warn-span col-xs-10 col-sm-8"></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right"> 身份证ID </label>
                        <div class="col-sm-9 warn-div">
                            <input type="text" name="id_card" value="{{ $info->id_card }}" class="col-xs-10 col-sm-8" maxlength="20" placeholder="格式:最大长度20"
                                   autocomplete="off">
                            <div class="warn-span col-xs-10 col-sm-8"></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right">性别 </label>
                        <div class="col-sm-9">
                            {!! From::radio($sexArray,isset($info->sex)?$info->sex:0,' name="sex" ',70,'sex') !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right">认证 </label>
                        <div class="col-sm-9">
                            {!! From::radio($certificationArray,isset($info->is_certification)?$info->is_certification:0,' name="is_certification" ',70,'is_certification') !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right">状态 </label>
                        <div class="col-sm-9">
                            {!! From::radio($statusArray,isset($info->status)?$info->status:1,' name="status" ',70,'status') !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right"> 用户邀请码 </label>
                        <div class="col-sm-9 warn-div">
                            <input type="text" name="invite_code" value="{{ $info->invite_code }}" class="col-xs-10 col-sm-8" maxlength="20" placeholder=""
                                   autocomplete="off" disabled>
                            <div class="warn-span col-xs-10 col-sm-8"></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right"> 推荐用户ID </label>
                        <div class="col-sm-9 warn-div">
                            <input type="text" name="recommend_id" value="{{ $info->recommend_id }}" class="col-xs-10 col-sm-8" maxlength="20" placeholder=""
                                   autocomplete="off" disabled>
                            <div class="warn-span col-xs-10 col-sm-8"></div>
                        </div>
                    </div>
                    <!-- 资产信息 -->


                    <div class="clearfix form-actions">
                        <div class="col-md-offset-3 col-md-9">
                            <button class="btn btn-info form-submit" type="button" id="dosubmit">
                                <i class="ace-icon fa fa-check bigger-110"></i>
                                提交
                            </button>
                            <button class="btn reset" type="reset">
                                <i class="ace-icon fa fa-undo bigger-110"></i>
                                Reset
                            </button>
                        </div>
                    </div>
                </form>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div>
    @include('admin.v1.common.img')

    <script>
        $(function(){
            $(".form-submit").on('click',function(){
                let data = {};
                let value = $('#form').serializeArray();
                let _status = true;
                let _names = [];
                $.each(value, function (index, item) {
                    data[item.name] = $.trim(item.value);
                    $("input[name="+item.name+"]").parent("div").find(".warn-span").html("");

                    if(item.name == 'password'){
                        var html = "";
                        var reg=/(?=^.{6,18}$)(?=.*\d)(?=.*[A-Z])(?=.*[a-z]).*$/;
                        if((item.value != null && item.value != '')){
                            if(!reg.test(item.value)){
                                // html = "<p>提示: 6-16位，,至少有一个数字，一个大写字母，一个小写字母和一个特殊字符，四个任意组合.</p>";
                                html = "<p>提示: 6-18位，至少有一个数字，一个大写字母，一个小写字母，三个任意组合.</p>";
                            }
                            if(html != ""){
                                $("input[name="+item.name+"]").parent("div").find(".warn-span").html(html);
                                _status = false;
                            }
                        }
                    }
                    if(item.name == 'email'){
                        var html = "";
                        var reg=/^[a-z\d]+(\.[a-z\d]+)*@([\da-z](-[\da-z])?)+(\.{1,2}[a-z]+)+$/;
                        if((item.value != null && item.value != '')){
                            if(!reg.test(item.value)){
                                html = "<p>提示: 邮件格式错误.</p>";
                            }
                            if(html != ""){
                                $("input[name="+item.name+"]").parent("div").find(".warn-span").html(html);
                                _status = false;
                            }
                        }
                    }

                });

                if(_status){
                    var index = layer.load(0, {shade: false});
                    $.ajax({
                        url:"/admin/v1/users/index/edit",
                        type:"post",
                        dataType:"json",
                        data:data,
                        success:function (response) {
                            layer.close(index);
                            if(response.status && response.code=='000000'){
                                var _data = response.data;
                                layer.msg(response.message, {icon: 1},function(){
                                    window.location.href = _data.url;
                                });

                            }else{
                                layer.alert(response.message,{
                                    icon: 2,
                                    title:'提示'
                                });
                            }
                        },
                        error:function(jqXHR, textStatus, errorThrown){
                            layer.close(layer.index);
                            if(jqXHR.status == 422 && textStatus == 'error'){
                                let responseError = jqXHR.responseJSON.errors;
                                $.each(responseError, function (index, item) {
                                    let html = "";
                                    $.each(item, function (key, val) {
                                        html += "<p>提示: "+val+"</p>";
                                    });
                                    $("input[name="+index+"]").parent("div").find(".warn-span").html(html);
                                })
                            }else if(jqXHR.status != 200){
                                layer.msg('请求错误', {
                                    icon: 2,
                                    // time: 20000, //20s后自动关闭
                                    btn: ['知道了']
                                });
                            }
                        }
                    });

                }
            });
            $(".reset").on('click',function(){
                $(".warn-span").html("");
            });
        });
    </script>
@endsection

