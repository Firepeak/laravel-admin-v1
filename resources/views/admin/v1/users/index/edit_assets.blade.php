@extends("admin.v1.common.main")
@section("content")

    <div class="page-content">


        <div class="page-header">
            <h1>
                <span>编辑</span>
                <div style="width: 50%; font-size: 0.7rem;display: inline-block;">
                    <a href="/admin"><span>主页</span></a>
                    @foreach($menu_parent as $items)
                        <a href="{{$items['path_url']}}"><span>/{{$items['menu_name']}}</span></a>
                    @endforeach
                </div>
                <button class="btn btn-sm btn-primary pull-right" onclick="javascript:window.location.href = 'lists'">
                    返回列表
                </button>
            </h1>

        </div><!-- /.page-header -->

        <div class="row">
            <div class="col-xs-12">

            @include("admin.v1.common.error")
            <!-- PAGE CONTENT BEGINS -->
                <form id="form" name="myform" class="form-horizontal" role="form" method="POST" action="create" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <input type="hidden" name="id" value="{{ $info->id }}">
                    <div class="form-group warn-div">
                        <label class="col-sm-3 control-label no-padding-right"> 账户 </label>
                        <div class="col-sm-9 warn-div">
                            <input type="text" name="account" value="{{ $info->account }}" class="col-xs-10 col-sm-6" minlength="2" maxlength="20" placeholder="格式:长度2～20"
                                   autocomplete="off" disabled="disabled">
                            <div class="warn-span col-xs-10 col-sm-8"></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right"> 资产信息 </label>
                        <div class="col-sm-9 warn-div">
                            <table id="simple-table" class="table table-bordered table-hover col-xs-10 col-sm-8" style="width:auto;">
                                <thead>
                                <tr>
                                    <th>资产名称</th>
                                    <th>总值</th>
                                    <th>可用值</th>
                                    <th>冻结值</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if($info->user_assets)
                                    @foreach($info->user_assets as $key=>$val)
                                        <tr>
                                            <td>{{ $val->name_alias }}</td>
                                            <td>{{ $val->total }}</td>
                                            <td>{{ $val->available }}</td>
                                            <td>{{ $val->freeze }}</td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="form-group warn-div">
                        <label class="col-sm-3 control-label no-padding-right"><code>*</code>选择资产 </label>
                        <div class="col-sm-9 warn-div">
                            <select class="form-control" style="max-width: 20rem;" name="select_assets">
                                <option value="" selected="">-- 请选择 --</option>
                                @if($info->user_assets)
                                    @foreach($info->user_assets as $key=>$val)
                                        <option value="{{ $val->id }}">{{ $val->name_alias }}</option>
                                    @endforeach
                                @endif
                            </select>
                            <div class="warn-span col-xs-10 col-sm-8"></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right"><code>*</code>资产类型</label>
                        <div class="col-sm-9">
                            {!! From::radio($assetsType,'available',' name="assets_type" ',70,'assets_type') !!}
                            <div style="color: #7a777a;">'冻结值'增减从'可用值'转换</div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right"><code>*</code>操作类型</label>
                        <div class="col-sm-9">
                            {!! From::radio($operationType,1,' name="operation_type" ',70,'operation_type') !!}
                            <div style="color: #7a777a;">冻结/解冻;冻结从'冻结值'转到'可用值',解冻</div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right"><code>*</code>变动值</label>
                        <div class="col-sm-9">
                            <input type="text" name="change_value" value="" class="col-xs-10 col-sm-3" maxlength="20" placeholder="格式:输入正数"
                                   autocomplete="off">
                            <div class="warn-span col-xs-10 col-sm-8"></div>
                        </div>
                    </div>



                    <div class="clearfix form-actions">
                        <div class="col-md-offset-3 col-md-9">
                            <button class="btn btn-info form-submit" type="button" id="dosubmit">
                                <i class="ace-icon fa fa-check bigger-110"></i>
                                提交
                            </button>
                            <button class="btn reset" type="reset">
                                <i class="ace-icon fa fa-undo bigger-110"></i>
                                Reset
                            </button>
                        </div>
                    </div>
                </form>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div>
    @include('admin.v1.common.img')

    <script>
        $(function(){
            $(".form-submit").on('click',function(){
                let data = {};
                let value = $('#form').serializeArray();
                let _status = true;
                $.each(value, function (index, item) {
                    data[item.name] = $.trim(item.value);
                    $("input[name="+item.name+"]").parent("div").find(".warn-span").html("");
                    $("select[name="+item.name+"]").parent("div").find(".warn-span").html("");

                    if(item.name == 'select_assets'){
                        var html = "";
                        if((item.value == null || item.value == '')){
                            html = "<p>提示: '选择资产'不能为空</p>";
                        }
                        if(html != ""){
                            $("select[name="+item.name+"]").parent("div").find(".warn-span").html(html);
                            _status = false;
                        }
                    }

                    if(item.name == 'change_value'){
                        var html = "";
                        var reg=/^[+]{0,1}(\d+)$|^[+]{0,1}(\d+\.\d+)$/;
                        if((item.value == null || item.value == '')){
                            html = "<p>提示: '变动值'不能为空</p>";
                        }else if(!reg.test(item.value)){
                            html = "<p>提示: '变动值'必须是正数</p>";
                        }
                        if(html != ""){
                            $("input[name="+item.name+"]").parent("div").find(".warn-span").html(html);
                            _status = false;
                        }
                    }

                });

                if(_status){
                    var index = layer.load(0, {shade: false});
                    $.ajax({
                        url:"/admin/v1/users/index/edit_assets",
                        type:"post",
                        dataType:"json",
                        data:data,
                        success:function (response) {
                            layer.close(index);
                            if(response.status && response.code=='000000'){
                                var _data = response.data;
                                layer.msg(response.message, {icon: 1},function(){
                                    window.location.href = _data.url;
                                });

                            }else{
                                layer.alert(response.message,{
                                    icon: 2,
                                    title:'提示'
                                });
                            }
                        },
                        error:function(jqXHR, textStatus, errorThrown){
                            layer.close(layer.index);
                            if(jqXHR.status == 422 && textStatus == 'error'){
                                let responseError = jqXHR.responseJSON.errors;
                                $.each(responseError, function (index, item) {
                                    let html = "";
                                    $.each(item, function (key, val) {
                                        html += "<p>提示: "+val+"</p>";
                                    });
                                    $("input[name="+index+"]").parent("div").find(".warn-span").html(html);
                                })
                            }else if(jqXHR.status != 200){
                                layer.msg('请求错误', {
                                    icon: 2,
                                    // time: 20000, //20s后自动关闭
                                    btn: ['知道了']
                                });
                            }
                        }
                    });

                }
            });
            $(".reset").on('click',function(){
                $(".warn-span").html("");
            });
        });
    </script>
@endsection

