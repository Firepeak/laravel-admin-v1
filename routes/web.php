<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//后台使用
Route::get('/admin', '\App\Http\Admin\Controllers\V1\AdminUser\LoginController@index');

Route::group([
    'namespace' => '\App\Http\Admin\Controllers\V1',
    'prefix' => 'admin/v1',
],function($router){
    // admin test
    $router->namespace('AdminTest')->prefix('test')->group(
        function ($router) {
            $router->get('menu', 'IndexController@menu');
        }
    );

    // admin common
    $router->namespace('AdminCommon')->prefix('common')->group(
        function ($router) {
            $router->get('msg', 'MsgController@publicIndex');
            $router->match(['get', 'post'],'index/generate_key', 'IndexController@generateKey'); // 生成随机key
            $router->match(['get', 'post'],'login_token', 'PublicController@loginToken'); // 登录的token
        }
    );

    // admin home
    $router->namespace('AdminHome')->prefix('home')->group(
        function ($router) {
            $router->get('index', 'IndexController@publicIndex');
        }
    );

    // admin user
    $router->namespace('AdminUser')->prefix('admin_user')->group(
        function ($router) {
            // 登录
            $router->get('login', 'LoginController@index');
            $router->post('login', 'LoginController@login');
            $router->match(['get', 'post'],'logout', 'LoginController@logout');
            // 日志
            $router->get('log/lists', 'LogController@lists');
            $router->get('log/info', 'LogController@info');
            // 管理员分组
            $router->get('group/lists', 'GroupController@lists');
            $router->get('group/info', 'GroupController@info');
            $router->match(['get', 'post'],'group/create', 'GroupController@create');
            $router->match(['get', 'post'],'group/edit', 'GroupController@edit');
            $router->delete('group/del', 'GroupController@del');
            // 管理员
            $router->get('index/lists', 'IndexController@lists');
            $router->get('index/info', 'IndexController@info');
            $router->match(['get', 'post'],'index/create', 'IndexController@create');
            $router->match(['get', 'post'],'index/edit', 'IndexController@edit');
            $router->match(['get', 'post'],'index/pwd', 'IndexController@pwd');
            $router->delete('index/del', 'IndexController@del');

            $router->match(['get', 'post'],'index/my_edit', 'IndexController@myEdit');
            $router->match(['get', 'post'],'index/my_pwd', 'IndexController@myPwd');

        }
    );

    // admin system
    $router->namespace('AdminSystem')->prefix('system')->group(
        function ($router) {
            // menu
            $router->get('menu/lists', 'MenuController@lists');
            $router->match(['get', 'post'],'menu/create', 'MenuController@create');
            $router->match(['get', 'post'],'menu/edit', 'MenuController@edit');
            $router->delete('menu/del', 'MenuController@del');
            $router->get('menu/info', 'MenuController@info');
            // config
            $router->get('config/index', 'ConfigController@index');
            $router->post('config/save', 'ConfigController@save');
            $router->get('config/clean_cache', 'ConfigController@cleanUpViewCache'); // 清理缓存
        }
    );

});
